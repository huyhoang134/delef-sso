var abo_press_valid_Exts = new Array(".xlsx", ".xls", ".csv", ".xl");


jQuery(document).ready(function() {
	
	
	$('form').submit(function( event ) {

		
		if($('.abo-press').length && $('.abo-press-titre-line').length==0 && $('.abo-press-site-line').length==0 && $('.abo-press-line').length==0 
			&&	 ( $('.wabo_fichier_name').length==0 ||  $('.wabo_fichier_name').val().length < 1 )  && $('.wabo_fichier_media_key').length && $('.wabo_fichier_media_key').val().length <1	&& $("input[type=file]").val().length <1 
		){
			
			if(!confirm("Vous n'avez pas déclaré d'abonements. Confirmez vous cette déclaration ?"))
			{
				event.preventDefault();
			}
			
		}
		
		if($('.abo-press').length && ( $('.abo-press-titre-line').length + $('.abo-press-site-line').length)>0 &&  $('.custom-file-upload-filename-text').val().length>1		
		)
		{
			if(!confirm("Vous avez à la fois chargé un fichier Excel et saisi des titres. C'est votre fichier Excel qui sera pris en compte. \n(si vous supprimez votre fichier Excel, c'est votre saisie qui sera prise en compte)"))
			{
				event.preventDefault();
			}
		}
		
	});
	
	

	
	if($('.abo-press').length){
		abo_press_titre_press_init();

		
		
		if($('.wabo_fichier_media_key').length){
			if($('.wabo_fichier_media_key').val().length>32)
			{
				$('.custom-file-upload-filename-text').val($('.wabo_fichier_name').val());
			
				$('.delete_file').show();
			}
		}
	
	}
});




function abo_press_add_titre_press(item){
	if($(item).closest('.abo-press-titre-line').find('input').val().length){
		$('.abo-press-titre-container-line').append(abo_livre_titre_line);
		abo_press_titre_press_init();
	}else{
		$('#err-presse').html("Vous devez reseigner un titre de presse, ou supprimer celui-ci.");
	}
	
	
}

function abo_press_add_site_press(item){
	
	if($(item).closest('.abo-press-site-line').find('input').val().length){
		$('.abo-press-site-container-line').append(abo_livre_site_line);
		abo_press_titre_press_init();
	}else{
		$('#err-presse-site').html("Vous devez reseigner un site de presse, ou supprimer celui-ci.");
	}

}

function abo_press_add_first_press()
{
	reset_abo_press_upload();
	if(($("input[type=file]").val().length  + $("input[name=wabo_fichier_name]").val().length)==0)
	{
		$('.abo-press-titre-container-line').append(abo_livre_titre_line);
		
		abo_press_titre_press_init();
		
	}


}

function abo_press_add_first_site()
{
	reset_abo_press_upload();
	if(($("input[type=file]").val().length + $("input[name=wabo_fichier_name]").val().length)==0)
	{
		$('.abo-press-site-container-line').append(abo_livre_site_line);
		abo_press_titre_press_init();
	}



}



function abo_press_delete_titre_press(item){
	$('#err-presse').html('');
	if($(item).closest('.abo-press-titre-line').find('input').val().length){
		if(confirm('Voulez-vous supprimer ce titre de presse ?')){
			$(item).closest('.abo-press-titre-line').remove();
			abo_press_titre_press_init();
			
		}
	}else{
		$(item).closest('.abo-press-titre-line').remove();
		abo_press_titre_press_init();
	}
	
}


function abo_press_delete_site_press(item){
	$('#err-presse-site').html('');
	if($(item).closest('.abo-press-site-line').find('input').val().length){
		if(confirm('Voulez-vous supprimer ce site de presse ?')){
			$(item).closest('.abo-press-site-line').remove();
			abo_press_titre_press_init();
		}
	}else{
		$(item).closest('.abo-press-site-line').remove();
		abo_press_titre_press_init();
	}

}

function abo_press_titre_press_init()
{

	if($('.abo-press-site-line:visible').length==0)
	{
		$('.add-first-site').show();
	}else{
		$('.add-first-site').hide();
	}
	if($('.abo-press-titre-line:visible').length==0)
	{
		$('.add-first-press').show();
	}else{
		$('.add-first-press').hide();
	}

	if($('.abo-press-line:visible').length==0)
	{
		$('.add-first-livre').show();
	}else{
		$('.add-first-livre').hide();
	}
	
	$('.wabop_site').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications-site/?',
	    onSelect: function (suggestion) {}
	});
	
	$('.wabop_titre').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications-ipro/?',
	    onSelect: function (suggestion) {}
	});
	
	
	
	$('.abo-press input[type="file"]').change(function(e){
		var fileName = e.target.files[0].name;
	    var sender = e.target.files[0];
	    
		//var panorama_index=$(this).closest('.panorama_press_numerique').data('index');

	    fileName = fileName.substring(fileName.lastIndexOf('.'));
	    if (abo_press_valid_Exts.indexOf(fileName) < 0) {
	    	alert("Type de fichier invalide. Veuillez séléctionner un fichier de type " + abo_press_valid_Exts.toString() + ".");
	    	$(e.target).parent().find(".custom-file-upload-filename input").val(abo_press_valid_Exts.toString());
	    	$(e.target).parent().find(".delete_file").hide();
	    }else{

	    	if($('.abo-press-titre-line').length || $('.abo-press-site-line').length || $('.abo-press-line').length){
	    		
	    		if(confirm("Le chargement d'un fichier pour vos abonnements et achats de presse nécessite la suppression des publications précédement renseignées.\r\nConfirmez-vous la supression des publications déclarées  ?"))
		    	{
		    		$(".custom-file-upload-filename input").val(e.target.files[0].name);
		    		$(".delete_file").show();
		    		$(".abo-press-titre-container-line").html("");
		    		$(".abo-press-site-container-line").html("");
		    		$(".abo-press-container-line").html("");
		    		
		    		$('#err-livre').html("");
		    		$('#err-presse').html("");
		    		
		    		$('.add-first-press').show();
		    		$('.add-first-site').show();
		    		$('.add-first-livre').show();
		    	}else{
			    	$(e.target).parent().find(".custom-file-upload-filename input").val(abo_press_valid_Exts.toString());
			    	$(e.target).parent().find(".delete_file").hide();
		    	}
	    	}else{
	    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
	    		$(e.target).parent().find(".delete_file").show();
	    	}
	    	
			
	    }
	});
}

function abo_press_reset_abo_press_upload()
{
	if(
			($("input[type=file]").val().length > 0 || $("input[name=wabo_fichier_name]").val().length > 0) 
			
			&& confirm("L'ajout d'une publication nécessite la suppresion du fichier précédement chargé.\r\nConfirmez-vous la supression du fichier chargé ?"))
	{
		$(".custom-file-upload-filename input").val(panorama_valid_Exts.toString());
		$("input[type=file]").val('');
		$(".delete_file").hide();
		$("input[name=wabo_fichier_media_key]").val("");
		$("input[name=wabo_fichier_name]").val("");
		//$('.wabo_fichier_name').val().length < 1 && $('.wabo_fichier_media_key').val()
	}
	
}

function add_first_pano_pub_num(item){
	var panorama_index=$(item).closest('.panorama_press_numerique').data('index');
	if($(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val().length==0 && ($(".panorama_press_numerique[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").length==0 || $(".panorama_press_numerique[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val().length==0)){
		$('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications').append(pano_pub_line);
		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	if(
			($(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val().length > 0 || $(".panorama_press_numerique[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val().length > 0) 
			
			&& confirm("L'ajout d'une publication nécessite la suppresion du fichier précédement chargé.\r\nConfirmez-vous la supression du fichier chargé pour ce panorama ?"))
	{
		$(".panorama_press_numerique[data-index="+panorama_index+"] .custom-file-upload-filename input").val(panorama_valid_Exts.toString());
		$(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val('');
		$(".panorama_press_numerique[data-index="+panorama_index+"] .delete_file").hide();
		$(".panorama_press_numerique[data-index="+panorama_index+"]  input[name=wppl_fichier_media_key]").val("");
		$(".panorama_press_numerique[data-index="+panorama_index+"]  input[name=wppl_fichier_name]").val("");
		$('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications').append(pano_pub_line);
		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	init_ppp_autocomplete();
}
