jQuery(document).ready(function() {
	
	//close_relation_publique_all();
});




function add_first_relation_publique()
{
	if($('.page-logged div#container_relation_publique > div:nth-last-of-type(1) .add-panorama a').length){
		$('.page-logged div#container_relation_publique > div:nth-last-of-type(1) .add-panorama a').trigger( "click" );
	}else{
		$('#container_relation_publique').append(relation_publique_line);
	}
	rebuild_relation_public_client_label();
}



function add_relation_publique(item){

	//console.log($(item).closest('.relation_publique').find('.wrp_label').val().length);
	//console.log($(item).closest('.relation_publique').find('.wrp_volume').val().length);
	if($(item).closest('.relation_publique').find('.wrp_label').val().length>0 && $(item).closest('.relation_publique').find('.wrp_volume').val().length>0 ){
		$('#container_relation_publique').append(relation_publique_line);
		$('.relation_publique_error').html("");
		//abo_press_titre_press_init();
	}else{
		$('.relation_publique_error').html("Vous devez renseigner un nom de client ainsi que le volume d’articles ou supprimer ce client");
	}
	rebuild_relation_public_client_label();
}

function rebuild_relation_public_client_label()
{
	$('.page-logged div#container_relation_publique .label-panorama-press ').each(function(i,v){
		  $(this).html('CLIENT ' + (i+1));
		});
}


function delete_relation_publique(item){
	if(confirm('Voulez-vous supprimer ce client ?')){
		$(item).closest('.relation_publique').remove();
	}
	$('.relation_publique_error').html("");
	rebuild_relation_public_client_label();
}


function close_relation_publique_all()
{
	
	$('.relation_publique .relation_publique_content:visible').each(function(){
		 if($(this).closest('.relation_publique').find('.relation_publique_content').is(":visible")){
		    	$(this).closest('.relation_publique').find(".ico-expend-collapse").removeClass('ico-expend');
		    	$(this).closest('.relation_publique').find(".ico-expend-collapse").addClass('ico-collapse');
		    }else{
		    	$(this).closest('.relation_publique').find(".ico-expend-collapse").removeClass('ico-collapse');
		    	$(this).closest('.relation_publique').find(".ico-expend-collapse").addClass('ico-expend');
		    }
		$(this).closest('.relation_publique').find('.relation_publique_content').toggle("blind", {},500, function() {});
	});
}


function toggle_relation_publique(item){
	

	//close_relation_publique_all();

	if($(item).closest('.relation_publique').find('.relation_publique_content').is(":visible")==false){
		
		if($(item).closest('.relation_publique').find('.relation_publique_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		
		$(item).closest('.relation_publique').find('.relation_publique_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.relation_publique').find('input:invalid').length)
		    {
		    	$( $(item).closest('.relation_publique').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}else{
		if($(item).closest('.relation_publique').find('.relation_publique_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		
		$(item).closest('.relation_publique').find('.relation_publique_content').toggle("blind", {},500, function() {
	    
		    if($(item).closest('.relation_publique').find('input:invalid').length)
		    {
		    	$( $(item).closest('.relation_publique').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
			    
		});
	}



}