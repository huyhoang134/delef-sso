/*Prevent multiple alerte with same msg and stop alert if clicked on cancel*/
var alert_array=[];
var alert_cancel=false;
var chorus_pro_siret_selected=false;
var modal_return=false;

jQuery(document).ready(function() {
	$('form').submit(function( event ) {
		alert_array=[];
		alert_cancel=false;
	});
});


jQuery(document).ready(function() {
	
	/*Uniquement pour FF*/
	/*if(navigator.userAgent.indexOf("Firefox") != -1 && $('#container_panorama_press_numerique > div').length=0){
		$(window).scroll(function(){
			console.log('scroll');
			if($('input:invalid, select:invalid').length){
				$('input:invalid, select:invalid').eq(0).focus();
			    if($('input:invalid, select:invalid').eq(0)[0].checkValidity()==false)
			    {
			    	$('#form_submit').trigger('click');
			    }
			}
		});
		
	}*/


	$('input[type=tel]').each(function() {

		//$(this).format_phone();
		$(this).intlTelInput({

			//autoPlaceholder: "polite",
			localizedCountries: { 
				'de': 'Allemagne' 
			},
			onlyCountries: [ "none",'at','be', 'bg','cy', 'hr','dk','es', 'ee','fi','fr', 'pf','de','gr', 'hu', 'is','ie','it','lv','li','lt','lu','mt','mq','gp','gf', 'no','nl', 'pl', 'pt', 'cz', 'ro', 'sk','si', 'se','mc','wf', 'nc'  ],
			preferredCountries: [ "none", "fr",'gp','gf', "pf", 'mq','nc','wf' ]
		});
	})

	$('input').each(function() {
		if($(this).attr('data-group-sum-ref')){
			$(document).group_sum_id( $(this) );
		}
	})
	
	
	var not_empty_groups = [];
	$('input').each(function() {
		if($(this).attr('data-group-1-not-empty-id')){
			not_empty_groups.push($(this).attr('data-group-1-not-empty-id'));
		}
	})
	$(document).group_1_not_empty(not_empty_groups);
	
	
	var if_one_then_all_group = [];
	$('input').each(function() {
		if($(this).attr('data-group-if-one-then-all-id')){
			//console.log("split");
			//console.log();
			
			//if_one_then_all_group.push($(this).attr('data-group-if-one-then-all-id'));
			var groups=$(this).attr('data-group-if-one-then-all-id').split(',');
			//console.log($(groups));
			
			$(groups).each(function(idx,item_group) {
				if(if_one_then_all_group.includes(item_group.trim())==false){
					if_one_then_all_group.push(item_group.trim());
				}
				
				
			});
			//console.log("end split");
		}
	})

	$(document).if_one_then_all(if_one_then_all_group);
	
	
	
	
	
	
	
	
	
	$('input').each(function() {
		if($(this).attr('data-not-empty-alert')){
			$(document).not_empty_alert($(this));
		}
	})
	
	
	var group_option_ids = [];
	$('input').each(function() {
		if($(this).attr('data-group-option-id')){
			group_option_ids.push($(this).attr('data-group-option-id'));
		}
	})
	$(document).group_option(group_option_ids);
	
	$('.alert_change_list').each(function() {
		$(document).alert_change_list( $(this) );
	});
	

	//compos-line
	$('form').submit(function( event ) {
		if( jQuery('.wcompos').length){
			$('.compos-errors').html("");
			jQuery('.wcompos').each(function(){
				if($(this).find('input').eq(0).val().length==0 || $(this).find('input').eq(1).val().length==0){
					$('.compos-errors').html(wcompos_msg_err_vide);
					event.preventDefault();
				}
			})
			
			if( jQuery('.etudiants_categorie').length && jQuery('.compos-total-bloc-total').length && parseInt(jQuery('.compos-total-bloc-total').html())!=jQuery('.etudiants_categorie').val()){
				$('.compos-errors').html("Attention ! Le nombre total d'étudiants doit être identique à la somme des effectifs par composantes");
				event.preventDefault();
			}
		}
		if( ( jQuery('.compos-stages').length==0 &&  jQuery('.compos-total-bloc-total').length && jQuery('.wcompos').length==0)){ 
			$('.compos-errors').html("Vous devez renseigner le nombre d’étudiants par composante.");
			event.preventDefault();
		}
		
		
	});
	
	
	//compos_stage not_empty_alert
	$('form').submit(function( event ) {
		if(typeof compos_stage_not_empty_alert !== 'undefined'  && $('.compos-stages .compos-line').length==0 ){
			if(!display_confirm(decodeHtml(compos_stage_not_empty_alert)))
			{
				event.preventDefault();
			}
		}
	});
	
	$('form').submit(function( event ) {
		
		if( jQuery('input.rgpd').length){
			if(!jQuery('input.rgpd').is(':checked')){
				var r = confirm("Acceptez-vous de recevoir des documents d’information et de communication du CFC ?\r\nOK : vous acceptez\r\nAnnuler : vous refusez ");
				if (r == true) {
					jQuery('input.rgpd').trigger('click');
					event.preventDefault();
				} else {
				  //non, on passe.
				} 
			}

		}
		
	});
	
	
	//slide to the first error
	$(document).scrollToError();

});




$.delefconfirm = function(title, content, label_cancel, label_confirm, invert_btn) {

    if($('.delef-modal').length){
		$('.delef-modal').remove()
	}
	if(invert_btn){
		var dialog = $('<div id="delef-modal" class="delef-modal"><div class="modal-content"><span class="modal-close">&times;</span><span class="modal-title"></span><p class="modal-text"></p><div><button type="submit" id="modal_cancel" name="modal_cancel">VALIDER</button><button type="submit" id="modal_confirm" name="modal_confirm">VALIDER</button></div></div></div>');
	}else{
		var dialog = $('<div id="delef-modal" class="delef-modal"><div class="modal-content"><span class="modal-close">&times;</span><span class="modal-title"></span><p class="modal-text"></p><div><button type="submit" id="modal_confirm" name="modal_confirm">VALIDER</button><button type="submit" id="modal_cancel" name="modal_cancel">VALIDER</button></div></div></div>');
	}
    var deferred = new $.Deferred();
    $(dialog).on("click", "#modal_confirm", deferred.resolve).on("click", "#modal_cancel", deferred.reject);
    $(document.body).append(dialog);
	$('.modal-title').html(title);
	$('.modal-text').html(content);
	$('#modal_cancel').html(label_cancel);
	$('#modal_confirm').html(label_confirm);
	$('.modal-close').hide();
    dialog.show();
    deferred.always(function() {
        dialog.hide();
    });

    return deferred.promise();
};


jQuery(document).ready(function() {
	
if($('.compos').length){
	$(compos_data).each(function(){
		$('.compos').append(compo_line);
		$('.compos .compos-line:nth-last-of-type(1) .col-compos-title-field input').val(decodeHtml($(this).attr('label')));
		$('.compos .compos-line:nth-last-of-type(1) .col-compos-num-field input').val($(this).attr('value'));

	});
	if(!$('.compos .compos-line').length){
		$('.compos').append(compo_line);
	}
	calculatecompos();
}	
	
	///compos_stages
if($('.compos-stages').length){
	$(compos_stages_data).each(function(){
		$('.compos-stages').append(compos_stages_line);
		$('.compos-stages .compos-line:nth-last-of-type(1) .compos-title-field input').val(decodeHtml($(this).attr('label')));
		$('.compos-stages .compos-line:nth-last-of-type(1) .compos-stagiaire-field input').val($(this).attr('effectif'));
		$('.compos-stages .compos-line:nth-last-of-type(1) .compos-heures-field input').val($(this).attr('heures'));
	});
	if(!$('.compos-stages .compos-line').length){
		$('.compos-stages').append(compos_stages_line);
	}
	
	
	calculatecompos_stages();
}	

});









/*compos*/

function addfirstcompos(){
	$('.compos-errors').html("");
	if($('.compos-line').length)
	{
		addcompos($('.compos-line .add-compos a').eq($('.compos-line').length-2));
	}else{
		$('.compos').append(compo_line);
	}
}
function addcompos(item){

	var compos_title=$(item).parent().parent().find('input').eq(0).val();
	var compos_num=$(item).parent().parent().find('input').eq(1).val();
	//console.log($(item).parent().parent().find('input').eq(0));
	//console.log(compos_title);
	//console.log(compos_num);
	if(compos_num==""){
		//$('.compos-errors').html("Merci de renseigner les effectifs de la composante ou de supprimer la ligne.");
		$('.compos-errors').html(wcompos_msg_err_vide);
	}else if(compos_title==""){
			//$('.compos-errors').html("Merci de renseigner l'intitulé de la composante ou de supprimer la ligne.");
			$('.compos-errors').html(wcompos_msg_err_vide);
			
	}else{
		$('.compos-errors').html("");
		$('.compos').append(compo_line);
		 $('.col-compos-num-field input').on('input', function() {
			 calculatecompos();
		});
	}
	
	
}

function removecompos(item){
	$(item).parent().parent().remove();
	$('.compos-errors').html("");
}

function calculatecompos(){
	var sum=0;
	$('.col-compos-num-field input').each(function(){
		var val=parseInt($(this).val())||0;
		sum = sum + parseInt(val);
	})
	$('.compos-total-bloc-total').html(sum);

}
(function($) {

	
	/*$("input[type=number]").bind({
	    keydown: function(e) {
	        if (e.shiftKey === true ) {
	            if (e.which == 9) {
	                return true;
	            }
	            return false;
	        }
	        if (e.which > 57) {
	            return false;
	        }
	        if (e.which==32) {
	            return false;
	        }
	        return true;
	    }
	});*/

	
	
	$.fn.alert_change_list = function(item) {
		var valueSelectDefault = $(item).val();
		$(item).change(function () {
	        if (valueSelectDefault == '')
	            return;

	        if (confirm($(item).data('alert-change-list-msg'))) {
	            valueSelectDefault = $(this).val();
	        } else
	            $(this).val(valueSelectDefault);
	    });
	};


	
	$.fn.group_option = function(item) {
		
		var groups=$.uniqueSort(item);
		$(groups).each(function(i,groupId){
			console.log(groupId);
			console.log($('.'+groupId));
			$('.'+groupId).closest('form').submit(function( event ) {
				$('input.'+groupId).each(function() {
					if($(this).attr('data-group-option-msg')){
						$('.err-data-group-option-msg').remove();
					}
				})
				
				var sum=0;
				$('input.'+groupId).each(function() {
					
		            if($(this).prop("checked") == true){
		            	sum=sum+1;
		            }
				})

				if(sum>1){
					
				  $('input.'+groupId).each(function() {
					  console.log($(this).attr('data-group-option-msg'));
						if($(this).attr('data-group-option-msg')){
							$('#err-'+$(this).attr('name')).append('<li class="err-data-group-option-msg"> '+$(this).attr('data-group-option-msg')+'</li>').show();
						}
					});
				  console.log("group_option");
					event.preventDefault();
				}
				
			});	
		});	
	}
	
	
	
	
	$.fn.not_empty_alert = function(item) {
		$('form').submit(function( event ) {
			var val=$(item).val()||0;
			if(val==0){
				return_alert=display_confirm( $(item).attr('data-not-empty-alert') );
				if (!return_alert) {
					console.log("blocked not_empty_alert => "+return_alert)
					event.preventDefault();
				}else{
					console.log("submit => "+return_alert);
				}
			}

			
		});
	}
	
	
	$.fn.scrollToError = function() {
		$('form').submit(function( event ) {
			if($('li[class^="err-"]').length)
			{
				$('html, body').animate({
			        scrollTop: $('li[class^="err-"]').eq(0).offset().top - 50
			    }, 1000);
			}
			
		});
	}
	
	
	
	$.fn.if_one_then_all = function(item) {
		
		
		
		var groups=$.uniqueSort(item);
		
		console.log("Groups : ");
		console.log(groups);
		
		$(groups).each(function(i,groupId){
			$('.'+groupId).closest('form').submit(function( event ) {
				$('input.'+groupId).each(function() {
					$('#err-'+$(this).attr('name')+' .err-data-group-if-one-then-all-message').remove();
				});
			});
			
		});
		
		$(groups).each(function(i,groupId){
			console.log("######################");
			console.log("if_one_then_all : "+groupId);
			console.log("######################");
			
			$('.'+groupId).closest('form').submit(function( event ) {
				$('input.'+groupId).each(function() {
					//$('#err-'+$(this).attr('name')+' .err-data-group-if-one-then-all-message').remove();
				});
			var sum=0;
			$('input.'+groupId).each(function() {
				if($(this).data("group-if-one-then-all-target")){
					var targets = $(this).data("group-if-one-then-all-target").split(',');
				}else{
					var targets = [];
				}
				
					if($(this).is('input[type=checkbox]')){
						if($(this).is('input[type=checkbox]:checked')){
							sum = sum + 1;
						}
					}else if($(this).is('input[type=radio]')){  
					}else{
						if($(this).val()!='' && targets.includes(groupId)==false ) {
							sum = sum + 1;
							console.log('if_one_then_all_group field remplis :')
							console.log($(this));
						}
						
					}
					
			})
			console.log('if_one_then_all_group remplis: '+ sum);
			
			prevent_default=false;
			if(sum>0){
				//alert(sum + ' groupId' + groupId);
			  $('input.'+ groupId).each(function() {

				  if($(this).is('input[type=checkbox]')){
				  }else if($(this).is('input[type=radio]')){  
					  if( $('input[name='+$(this).attr('name')+']:checked').length==0 ){
						$('#err-'+$(this).attr('name')+' .err-data-group-if-one-then-all-message').remove();
						$('#err-'+$(this).attr('name')).append('<li class="err-data-group-if-one-then-all-message"> '+$(this).attr('data-group-if-one-then-all-message')+'</li>').show();
						prevent_default=true;
						}
				  }else if($(this).is('input[type=tel]')){  
					  if( $(this).val().length<=9 ){
						  
						$('#err-'+$(this).attr('name')+' .err-data-group-if-one-then-all-message').remove();
						$('#err-'+$(this).attr('name')).append('<li class="err-data-group-if-one-then-all-message"> '+$(this).attr('data-group-if-one-then-all-message')+'</li>').show();
						prevent_default=true;
						}  
				  }else{
					  if($(this).val()=="" && $(this).attr('data-group-if-one-then-all-message')){
						$('#err-'+$(this).attr('name')+' .err-data-group-if-one-then-all-message').remove();
						$('#err-'+$(this).attr('name')).append('<li class="err-data-group-if-one-then-all-message"> '+$(this).attr('data-group-if-one-then-all-message')+'</li>').show();
						prevent_default=true;
					  }
				  }
					
				})
			}
			
			if(prevent_default){
				event.preventDefault();
			}
			
			});
		})
	};
	
	
	
	
	$.fn.group_1_not_empty = function(item) {
		console.log("group_1_not_empty");
		
		var groups=$.uniqueSort(item);

		$(groups).each(function(i,groupId){
			$('.'+groupId).closest('form').submit(function( event ) {
				$('input.'+groupId).each(function() {
					if($(this).attr('data-group-1-not-empty-message')){
						$('.err-data-group-1-not-empty-message').remove();
					}
				})
			var sum=0;
			$('input.'+groupId).each(function() {
				
					if($(this).is('input[type=checkbox]')){
						if($(this).is('input[type=checkbox]:checked')){
							sum = sum + 1;
						}
					}else{
						sum = sum + (parseInt($(this).val())||0);
					}
					
			})
			prevent_default=false;
			if(sum==0){
			  $('input.'+groupId).each(function() {
					if($(this).attr('data-group-1-not-empty-message')){
						//$('#err-'+$(this).attr('name')).html($(this).attr('data-group-1-not-empty-message')).show();
						$('#err-'+$(this).attr('name')).append('<li class="err-data-group-1-not-empty-message"> '+$(this).attr('data-group-1-not-empty-message')+'</li>').show();
						prevent_default=true;
					}
					if($(this).attr('data-group-1-not-empty-alert')){
						if(!display_confirm($(this).attr('data-group-1-not-empty-alert')))
						{
							prevent_default=true;
						}	
					}
				})
				console.log('group_1_not_empty')
			}
			
			if(prevent_default){
				event.preventDefault();
			}
			
			});
		})
	};
	

	
	$.fn.group_sum_id = function(item) {
		console.log("group_sum_id");
		console.log(item);
		var groups = $(item).attr('data-group-sum-id') ,sum=0;
		$(groups.split(',')).each(function(i,groupId){
			console.log($.trim(groupId));
			groupId=$.trim(groupId);
			$('.'+groupId).closest('form').submit(function( event ) {
				console.log($('input.'+groupId));
				$('input.'+groupId).each(function() {
					console.log($(this));
					if($(this).attr('data-group-sum-msg')){
						var name=$(this).attr('name');
						name = name.replace("[", "");
						name = name.replace("]", "");
						$('#err-'+name).find('.err-data-group-sum-msg'+groupId).remove();
					}
				});
				
				
				var total = parseInt($(item).val())||0;
				var sum=0;
				  $('input.'+groupId).each(function() {
					  //console.log($('input.'+groupId));
					  //console.log('input.'+groupId);
					  
						if(!$(this).attr('data-group-sum-ref')){
							console.log((parseInt($(this).val())||0));
							sum = sum + (parseInt($(this).val())||0);
						}
					})
					
				  if(total!=sum){
					  console.log('group : '+'input.'+groupId+' => total : '+total+"=> sum : "+sum);
					  $('input.'+groupId).each(function() {
							if($(this).attr('data-group-sum-msg')){
								try{
									var error_message=$.parseJSON($(this).attr('data-group-sum-msg'));
								}catch(e){
									var error_message=$(this).attr('data-group-sum-msg');
								}

								
								if(typeof error_message =='object')
								{
									//value should be {"groupid_1":message 1","groupid_2":"message 2"}
								 // eval("var message=error_message."+groupId);
								  var message=error_message[groupId];
								  //console.log(error_message);
								 // console.log("groupId "+groupId);
								}else{
									var message=$(this).attr('data-group-sum-msg');
								}
								console.log('#err-'+$(this).attr('name'));
								var name=$(this).attr('name');
								name = name.replace("[", "");
								name = name.replace("]", "");
								if($('#err-'+name).find('.err-data-group-sum-msg'+groupId).length==0){
									$('#err-'+name).append('<li class="err-data-group-sum-msg'+groupId+'"> '+message+'</li>').show();
								}
								
								event.preventDefault();
							}
						})
						
						$('input.'+groupId).each(function() {
							;
							if($(this).attr('data-group-sum-alert-msg')){
								if(!display_confirm($(this).attr('data-group-sum-alert-msg')))
								{
									event.preventDefault();
								}else{
									return true;
								}
							}
						})
						
						console.log('group_sum_id')
						
				  }else{
					  
				  }
			});
		});
	};
	
	$.fn.format_phone = function() {
		$('input[type=tel]').each(function() {
			//$(this).format_phone();
			$(this).intlTelInput({
				autoPlaceholder: "polite",
				localizedCountries: { 
					'de': 'Allemagne' 
				},
				onlyCountries: [ "none", 'at','be', 'bg','cy', 'hr','dk','es', 'ee','fi','fr', 'pf','de','gr', 'hu', 'is','ie','it','lv','li','lt','lu','mt','mq','gp','gf', 'no','nl', 'pl', 'pt', 'cz', 'ro', 'sk','si', 'se','mc','wf', 'nc'  ],
				preferredCountries: ["none",  "fr",'gp','gf', "pf", 'mq','nc','wf' ]
			});
		})
		
		return;
		$(this).keyup(
						function(e) {
							var a = [];
							var k = e.which;
							var field = $(this), fieldVal = field.val(), removeSpace = fieldVal
									.replace(/ /g, ''), valLimit = removeSpace.length >= 10;
									if (($.inArray(e.keyCode, [ 46, 8, 9, 27, 13, 110,
									190, 32 ]) !== -1
									||
									// Allow: Ctrl+A
									(e.keyCode == 65 && e.ctrlKey === true) ||
							// Allow: home, end, left, right, down, up
							(e.keyCode >= 35 && e.keyCode <= 40))) {
								// let it happen, don't do anything
								//return true;
							}
							
							// Ensure that it is a number and stop the keypress
							/*if (((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
									&& (e.keyCode < 96 || e.keyCode > 105))) {
								console.log("format_phone");
								console.log(e.keyCode)
								e.preventDefault();
							}*/
							/*console.log('REt : '+$.inArray(e.keyCode, [16, 96,97,98,99,100,101,102,103,104,105,49,50,51,52,53,54,55,56,57 ]) )
							if ($.inArray(e.keyCode, [16, 96,97,98,99,100,101,102,103,104,105,49,50,51,52,53,54,55,56,57 ]) == -1) {
								console.log("format_phone");
								console.log("format_phone");
								
								e.preventDefault();
							}	*/	
									
							
							var fieldID = field.attr('id');
							fieldVal = fieldVal.replace(/[^0-9\.]/g, '');
							var strTemp = $.trim(fieldVal);
							
							var arrChar;
							arrChar = strTemp.split(' ');
							strTemp = arrChar.join('');
							arrChar = strTemp.split('');
							var count = 0;
							if (strTemp != '') {
								arrChar = strTemp.split('');
								for (var i = 0; i < arrChar.length; i++) {
									if (arrChar[i] != ' ' ) {
										if (count != 2) {
											count++;
										} else {
											arrChar.splice(i, 0, ' ');
											count = 0;
										}
									} else {
										count = 0;
									}
								}
								val = arrChar.join('');
								val = val.substr(0, 14);
								field.val(val);
							} else {
								field.val('');
								return;
							}

						});
	};

}(jQuery));

function decodeHtml(str)
{
    var map =
    {
        '&amp;': '&',
        '&lt;': '<',
        '&gt;': '>',
        '&quot;': '"',
        '&#039;': "'"
    };
    return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
}

/*function unescapeHtml(safe) {
    return safe.replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&#039;/g, "'");
}
*/
function display_confirm(msg){
	
	if($.inArray(msg, alert_array )==-1 && alert_cancel==false){
		alert_array.push(msg);
		var ret=confirm(msg) ;
		if(ret==false){
			alert_cancel=true;
		}
		return ret;
	}else{
		return true;
		console.log('alert_cancel || allready popped');
	}

}