
jQuery(document).ready(function() {
	
	if($('.file-upload-row').length){
		$('.file-upload-row').each(function(){
			var file_upload_bloc=this;
			var extenions=$(file_upload_bloc).find(".file-upload-filename-text").data('extentions');
			var extenions_arr=extenions.split(',');
			for (var i = 0; i < extenions_arr.length; i++) {
				extenions_arr[i] = extenions_arr[i].trim()
		    }
			
			if($(file_upload_bloc).find('.file-upload-fichier-media-key').val().length>32)
			{
				
				$(file_upload_bloc).find('.file-upload-filename-text').val($(this).find('.file-upload-fichier-name').val());
			
				$(file_upload_bloc).find('.delete_file').show();
			}
			
			$(file_upload_bloc).find('.file-upload-file-input').change(function(e){
			
				var fileName = e.target.files[0].name;
			    var sender = e.target.files[0];
			    fileName = fileName.substring(fileName.lastIndexOf('.'));
			    
			    if (extenions_arr.indexOf(fileName) < 0) {
			    	alert("Type de fichier invalide. Veuillez séléctionner un fichier de type " + extenions_arr.toString() + ".");
			    	$(e.target).parent().find(".file-upload-filename-text").val(extenions_arr.toString());
			    	$(e.target).parent().find(".delete_file").hide();
			    }else{
				    
				    $(file_upload_bloc).find(".file-upload-filename-text").val(e.target.files[0].name);
				    $(file_upload_bloc).find(".delete_file").show();
			    }
			    

	    		
			});

			
		})
	}
	

	
	
});




function file_upload_reset_upload(item)
{
	var file_upload_bloc=$(item).closest('.file-upload-row');
	var extenions=$(file_upload_bloc).find(".file-upload-filename-text").data('extentions');
	
	$(file_upload_bloc).find(".file-upload-filename-text").val(extenions);
	$(file_upload_bloc).find(".file-upload-fichier-media-key").val("");
	$(file_upload_bloc).find(".file-upload-fichier-name").val("");
	$(file_upload_bloc).find(".delete_file").hide();

}