jQuery(document).ready(function() {
	
	close_accordion_options_all();
	
	$('.accordion_options_field').on('input keyup', function (e) {
		console.log($(this));
			switch (e.keyCode) {
				case 38: // Up
					return;
					break;
				case 40: // Down
					break;
				case 13: // Enter
					
					accordion_options_check_required($(this));
					e.preventDefault();
					break;
				case 9:  // Tab
				case 27: // Esc
					break;
				default:
					accordion_options_check_required($(this));
					break;
			}

		
	});
	$('.accordion_options_field').each(function(){
		accordion_options_check_required(this);
	})
	
	

if($('.container_accordion_options').length){
	$( "#form_submit" ).click(function() {

    	if($('input:invalid').length)
    	{
    		if($('input:invalid').eq(0).closest('.container_accordion_options').find('.accordion_options_content').is(":hidden")){
    			
    			$('input:invalid').eq(0).closest('.container_accordion_options').find('.ico-expend-collapse a')[0].scrollIntoView(); 
    			$('input:invalid').eq(0).closest('.container_accordion_options').find('.ico-expend-collapse a').trigger('click');
    			$('input:invalid').eq(0).focus();
    			setTimeout(function(){ $( "#form_submit" ).trigger('click'); }, 800);
    			
    		}
    		
    	}
    });
}
    

	

	
});

function accordion_options_check_required(item)
{
	var item_value=$(item).val();
	if (item_value=="" || item_value==0)
	{
		$(item).val(0);
		$(item).closest('.container_accordion_options').find('input[type=radio]').prop('required',false);
	}else{
		$(item).closest('.container_accordion_options').find('input[type=radio]').prop('required',true);
		
	}
	
}

function close_accordion_options_all(){
	if($('.container_accordion_options').length){
		$('form[name=form] .dotted-line:last').hide();
	}
	
	$('.container_accordion_options .accordion_options_content:visible').each(function(){
		if($(this).closest('.container_accordion_options').find('.accordion_options_field').val()==0){
			$(this).closest('.container_accordion_options').find(".ico-expend-collapse").removeClass('ico-collapse');
	    	$(this).closest('.container_accordion_options').find(".ico-expend-collapse").addClass('ico-expend');
			$(this).closest('.container_accordion_options').find('.accordion_options_content').toggle("blind", {},500, function() {});
		}
    	
	});
}


function toggle_accordion_options(item){
	

	//close_panorama_all();

	if($(item).closest('.container_accordion_options').find('.accordion_options_content').is(":visible")==false){
		$(item).closest('.container_accordion_options').find(".ico-expend-collapse").removeClass('ico-expend');
    	$(item).closest('.container_accordion_options').find(".ico-expend-collapse").addClass('ico-collapse');
		$(item).closest('.container_accordion_options').find('.accordion_options_content').toggle("blind",{}, 500);
		
	}else{
		console.log('no visible');
		$(item).closest('.container_accordion_options').find(".ico-expend-collapse").removeClass('ico-collapse');
    	$(item).closest('.container_accordion_options').find(".ico-expend-collapse").addClass('ico-expend');
		$(item).closest('.container_accordion_options').find('.accordion_options_content').toggle("blind", {},500, function() {});
	}



}

