jQuery(document).ready(function() {



$( "form" ).each(function() {
    var form = this;

    // Suppress the default bubbles
    form.addEventListener( "invalid", function( event ) {
        event.preventDefault();
    }, true );

    // Support Safari, iOS Safari, and the Android browser—each of which do not prevent
    // form submissions by default
    $( form ).on( "submit", function( event ) {
        if ( !this.checkValidity() ) {
            event.preventDefault();
        }
    });

    $( "input, select, textarea", form )
        // Destroy the tooltip on blur if the field contains valid data
        .on( "blur", function() {
        	var field = $( this );
        	if (field.data && field.data('bs.tooltip')) {
        		if ( this.validity.valid ) {
        			$( this ).tooltip('dispose');
                } else {
                	$( this ).tooltip( "hide" );
                }
        	}

        })
        // Show the tooltip on focus
        .on( "focus", function() {
        	var field = $( this );
        	if (field.data && field.data('bs.tooltip')) {
        		$( this ).tooltip('show');
        	}
        });

    $( "button:not([type=button]), input[type=submit]", form ).on( "click", function( event ) {
        // Destroy any tooltips from previous runs
        $( "input, select, textarea", form ).each( function() {
        	$( this ).tooltip('dispose');
        });

        // Add a tooltip to each invalid field
        var invalidFields = $( ":invalid", form ).each(function() {
        	if (!$( this )[0].checkValidity()) {
	        	$( this ).tooltip({
	        		'title':$( this )[ 0 ].validationMessage,
	        		'placement': 'right',
	        		
	        		});
        	}
        	
        });

        // If there are errors, give focus to the first invalid field
        invalidFields.first().trigger( "focus" ).eq( 0 ).focus();
        //invalidFields.first().scrollIntoView();
        if(invalidFields.length){
            $('html, body').animate({
    	        scrollTop: invalidFields.first().offset().top - 50
    	    }, 1000);
        }

    });
});


});