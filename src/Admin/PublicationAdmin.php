<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

final class PublicationAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('publicationId', null, ['label' => 'Type ID contrat'])
            ->add('publicationTitre', null, [ 'label' => 'Titre'])
            ->add('publicationType', null, ['label' => 'Type'])
            ->add('publicationCountryCode', null,  ['label' => 'Code'])
            ->add('publicationRedevancePanorama', null, ['label' => 'Redevance Panorama'])
            ->add('publicationRedevanceClipping', null, ['label' => 'Redevance Clipping'])
            ->add('publicationArticlesAuthPanorama', null, [ 'label' => 'Articles Auth Panorama'])
            ->add('publicationInclusionInfographie', null, ['label' => 'Inclusion Infographie'])
            ->add('publicationDateEffetMandat', null, [ 'label' => 'Date Effet Mandat'])
            ->add('publicationObservation', null, [ 'label' => 'Observation'])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('publicationId', null, ['label' => 'ID', 'editable' => false , 'header_style' => 'width: 50px'])
            ->add('publicationTitre', null, ['label' => 'Titre', 'editable' => false , 'header_style' => ''])
            ->add('publicationType', null, ['label' => 'Type', 'editable' => false , 'header_style' => 'width: 150px'])
            ->add('publicationCountryCode', null, ['label' => 'Country Code', 'editable' => false , 'header_style' => 'width: 90px'])
            ->add('publicationRedevancePanorama', null, ['label' => 'Redevance Panorama', 'editable' => false , 'header_style' => 'width: 20px'])
            ->add('publicationRedevanceClipping', null, ['label' => 'Redevance Clipping', 'editable' => false , 'header_style' => 'width: 20px'])
            ->add('publicationArticlesAuthPanorama', null, ['label' => 'Articles Auth Panorama', 'editable' => false , 'header_style' => 'width: 20px'])
            ->add('publicationInclusionInfographie', null, ['label' => 'Inclusion Infographie', 'editable' => false , 'header_style' => 'width: 150px'])
            ->add('publicationDateEffetMandat', 'date', ['label' => 'Date Effet Mandat', 'editable' => false , 'header_style' => 'width: 150px'])
            ->add('publicationObservation', null, ['label' => 'Observation', 'editable' => false , 'header_style' => ''])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper->tab('General');
        $formMapper->with('Informations générales', ['class' => 'col-md-8']);
        
        $formMapper
            ->add('publicationTitre', TextType::class, ['required' => false,  'label'=>'Titre','attr' => ['placeholder' => '']])
            
            ->add('publicationObservation', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Observation','attr' => ['placeholder' => '']])
            ;
        
        $formMapper->end();
        
        $formMapper->with('Parametres', ['class' => 'col-md-4']);
        $formMapper->add('publicationDateEffetMandat', DateType::class, [ 'widget' => 'single_text','required' => false,  'label'=>'Date Effet Mandat' ,'attr' => ['placeholder' => '']]);
        $formMapper->add('publicationType', TextType::class, ['required' => false,  'label'=>'Type','attr' => ['placeholder' => '']]);
        $formMapper->add('publicationCountryCode', TextType::class, ['required' => false,  'label'=>'Country Code','attr' => ['placeholder' => '']]);
        $formMapper->add('publicationRedevancePanorama', TextType::class, ['required' => false,  'label'=>'Redevance Panorama','attr' => ['placeholder' => '']]);
        $formMapper->add('publicationRedevanceClipping', TextType::class, ['required' => false,  'label'=>'Redevance Clipping','attr' => ['placeholder' => '']]);
        $formMapper->add('publicationArticlesAuthPanorama', TextType::class, ['required' => false,  'label'=>'Articles Auth Panorama','attr' => ['placeholder' => '']]);
        
        $formMapper->add('publicationInclusionInfographie', TextType::class, [ 'required' => false,  'label'=>'Inclusion Infographie','attr' => ['placeholder' => '']]);
        $formMapper->end();
        
        $formMapper->end();
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('publicationId', null, ['label' => 'ID'])
            ->add('publicationTitre', null, ['label' => 'Titre'])
            ->add('publicationType', null, ['label' => 'Type'])
            ->add('publicationCountryCode', null, ['label' => 'Country Code'])
            ->add('publicationRedevancePanorama', null, ['label' => 'Redevance Panorama'])
            ->add('publicationRedevanceClipping', null, ['label' => 'Redevance Clipping'])
            ->add('publicationArticlesAuthPanorama', null, ['label' => 'Articles AuthPanorama'])
            ->add('publicationInclusionInfographie', null, ['label' => 'Inclusion Infographie'])
            ->add('publicationDateEffetMandat', 'date', ['label' => 'Date Effet Mandat'])
            ->add('publicationObservation', null, ['label' => 'Observation'])
            ;
    }
}
