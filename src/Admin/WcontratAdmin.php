<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Wcocon;
use App\Entity\Wcontrat;
use App\Entity\Wdeclar;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Provider\FileProvider;

final class WcontratAdmin extends AbstractAdmin
{



    protected function configureRoutes(RouteCollection $collection)
    {
        //add route for our custuom button
        $collection->add('declar', $this->getRouterIdParameter().'/show');
    }

    public function configureActionButtons($action, $object = null)
    {
        $buttons = parent::configureActionButtons($action, $object);
        if (in_array($action, array('list'))) {
            unset($buttons['create']);
        }

        return $buttons;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('wct_contrat')
            ->add('wctStamp')
            ->add('wctSynchro')
            ->add('wctDossier')
            ->add('wct_typcont')

            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper->add('_action', 'actions', [
            'header_style' => 'width: 50px',
            'actions' => [
                'declar' => [
                    'template' => 'Admin/list__action_declar.html.twig'
                ],

            ]
        ]);

        $listMapper
            ->add('wctContrat')
            ->add('wctStamp')
            ->add('wctSynchro')
            ->add('wctDossier');

    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('wctContrat')
            ->add('wctStamp')
            ->add('wctSynchro')
            ->add('wctDossier')
            ->add('wctTypcont')

            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
       $showMapper
            ->add('wctContrat')
            ->add('wctStamp')
            ->add('wctSynchro')
            ->add('wctDossier')
            ->add('wctTypcont');

            ;
    }

    public function createQuery($context = 'list')
    {
        if($context=='list'){


            $dossier=$this->getRequest()->get('childId');
            //Get current typecont
            //$typecont=parent::createQuery()->getEntityManager()->getRepository('App\Entity\Wtype')->findOneBy(['id'=>$id])->getLabel();
            $proxyQuery = parent::createQuery('list');
            $proxyQuery->leftJoin(
                'App\Entity\Wdelef',
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'o.wct_contrat = d.wdfContrat'
            );
            $proxyQuery->where('o.wctDossier  = :dossier');
            $proxyQuery->setParameter('dossier', $dossier);


        }


        return $proxyQuery;
    }

    public function get_declar_conf($Wcontrat){



        $admin = $this->isChild() ? $this->getParent() : $this;
        $wtype_id = $admin->getRequest()->get('id');


        $em = parent::createQuery()->getEntityManager();
        $WcoconRepository = $em->getRepository(Wcocon::class);
        $declaration_conf = $WcoconRepository->getDeclarationConfigurationByWcontrat($Wcontrat);

        $primary_declaration = $em->getRepository(Wcontrat::class)->findOneBy(["wct_contrat" => $Wcontrat->getWctContrat()]);

        $contrats=$WcoconRepository->getListContrats($declaration_conf);

        foreach($declaration_conf as $i=>$conf){

            foreach($conf as $j=>$c){



                if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                    $db_data_conf[$i]["wde_is_chorus_pro"]=$WcoconRepository->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    $db_data_conf[$i]["wde_siret"]=$WcoconRepository->getValue("wde_siret", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    $db_data_conf[$i]["wde_service_code"]=$WcoconRepository->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                }elseif($c->getTypeObjet()=="panorama_press_papier"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);

                }elseif($c->getTypeObjet()=="file_upload")
                {
                    $data=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    if(!empty($data)){
                        $mediaManager = $this->getConfigurationPool()->getContainer()->get('sonata.media.manager.media');
                        $media=$mediaManager->find($data);
                        $file_name=$media->getName();

                        $file_reference= $this->provider->getReferenceFile($media,'reference')->getName();
                        $media_key=md5($data.$WcoconRepository->download_key).'%'.$data;
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>$data,
                            'file_upload_fichier_name'=>$file_name,
                            'file_upload_fichier_reference'=>$file_reference,
                            'file_upload_fichier_media_key'=>$media_key,
                        ];
                    }else{
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>'',
                            'file_upload_fichier_name'=>"",
                            'file_upload_fichier_reference'=>"",
                            'file_upload_fichier_media_key'=>"",
                        ];
                    }
                }elseif($c->getTypeObjet()=="accordion_options")
                {
                    $TypeObjetConf=$c->getTypeObjetConf();
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar, $c->wdf_declar);

                }else{
                    if (!empty($c->getSynchroField())) {
                        $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                    }
                }
            }
        }

        return [
            'contrats'=>$contrats,
            'declaration_conf'=>$declaration_conf,
            'primary_declaration'=>$primary_declaration,
            'db_data_conf'=>$db_data_conf,
        ];
    }
}
