<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sonata\CoreBundle\Form\Type\DatePickerType;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface as RoutingUrlGeneratorInterface;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

use Sonata\AdminBundle\Route\RouteCollection;
#use Pix\SortableBehaviorBundle\Services\PositionORMHandler as PositionHandler;
use Sonata\AdminBundle\Controller\CRUDController;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;


class WtypeAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'id',
        '_sort_order' => 'ASC',
        '_per_page' => '256'
    );
    
    protected $perPageOptions = [16, 32, 64, 128, 192, 256];
    protected $maxPerPage = '128';
    
    private $activation_dates=array(
        '01 janvier' => '01 janvier',
        '01 septembre' => '01 septembre',
    );
    
    
    private $departements=array(
        'Département entreprises et administrations' => 'DEA',
        'Département enseignement et formation' => 'DEF',
    );
    
    /*private $priorite=array(
        'Primaire' => '0',
        'Secondaire' => '1',
    );*/
    private $email_signature=array(
        'Le Département Enseignement et Formation du CFC.' => 'Le Département Enseignement et Formation du CFC.',
        'Le Département Entreprises et Administrations' => 'Le Département Entreprises et Administrations',
    );
    
    private $archive=array(
        'Sans' => '0',
        'Dernière déclaration' => '1',
        'Archive' => '2',
    );
    private $archive_ouverture=array(
        'Pas d\'affichage' => '0',
        'Interface ouverte' => '1',
        'Interface fermée' => '2',
        'Interface ouverte et fermée' => '3',
    );
    
    private $type_interface=array(
        'Chainé' => '0',
        'Indépendantes' => '1',

    );
    
    
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );
        

    }
    
    public function configure()
    {
        parent::configure();
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->tab('General');
            $formMapper->with('Informations générales', ['class' => 'col-md-8']);
                //$formMapper->add('id', IntegerType::class, ['required' => false,  'label'=>'Id','attr' => ['placeholder' => '']]);
                $formMapper->add('label', TextType::class, ['required' => true,  'label'=>'Code synchro','attr' => ['placeholder' => '']]);
                $formMapper->add('label_interface', TextType::class, ['required' => true,  'label'=>'Label de l\'interface','attr' => ['placeholder' => '', ],'help'=>'Label affiché dans l\'interface. Utiliser le mot clef %wde_annee% pour placer la valeur du champs wde_annee dans la chaine. (champs possibles %wde_annee% , %wde_numperiod%) ']);
                $formMapper->add('label_interface_home', TextType::class, ['required' => false,  'label'=>'Label de l\'interface (sur la home contrats chainés uniquement)','attr' => ['placeholder' => '', ],'help'=>'Label affiché dans l\'interface. Utiliser le mot clef %wde_annee% pour placer la valeur du champs wde_annee dans la chaine. ']);
                $formMapper->add('description', TextType::class, ['required' => false,  'label'=>'Description','attr' => ['placeholder' => '']]);

                
            $formMapper->end();
            
                
            $formMapper->with('Contacts', ['class' => 'col-md-4 abcdaire'])
                ->add('contact_email', TextType::class, ['required' => false,  'label'=>'Email de contact','attr' => ['placeholder' => '' , 'rows'=>6]])
                ->add('contact_email2', TextType::class, ['required' => false,  'label'=>'Email de contact bis','attr' => ['placeholder' => '' , 'rows'=>6]])
                ->add('send_validation_email', CheckboxType::class, ['required' => false, 'label'=>'Envoyer la déclaration en email au contact'])
                
            ->end();
                
                
            $formMapper->with('Status', ['class' => 'col-md-4 abcdaire'])
                    ->add('actif', CheckboxType::class, ['required' => false])
                    ->add('departement', ChoiceType::class, ['choices' => $this->departements,'required' => true,   'label'=>'Département', 'attr' => ['placeholder' => ''] ])
                    ->add('priorite', IntegerType::class, ['required' => true,   'label'=>'Priorité du contrat', 'attr' => ['placeholder' => ''], 'help'=>"Permet de prioriser le contrat" ])
                    ->add('type_interface', ChoiceType::class, ['choices'=>$this->type_interface, 'required' => true,   'label'=>'Type de l\'interface', 'attr' => ['placeholder' => ''], 'help'=>"Permet de prioriser le contrat" ])
                    ->add('email_signature', ChoiceType::class, ['choices' => $this->email_signature,'required' => true,   'label'=>'Signature email', 'attr' => ['placeholder' => ''] ])
                    
            
            ->end();

            
            $formMapper->with('Date d\'activation', ['class' => 'col-md-4 abcdaire'])
                    ->add('activation_civile', ChoiceType::class, ['choices' => $this->activation_dates,'required' => true,   'label'=>'Activation année civile', 'attr' => ['placeholder' => ''] ])
                    ->add('activation_scolaire', ChoiceType::class, ['choices' => $this->activation_dates,'required' => true,  'label'=>'Activation année scolaire','attr' => ['placeholder' => '']])
            ->end();

            $formMapper->with('Acces démonstration', ['class' => 'col-md-4 abcdaire'])
                ->add('comptes_demo', TextareaType::class, ['required' => false,  'label'=>'Comptes de teste','attr' => ['placeholder' => '' , 'rows'=>6]])
           ->end();
                
                
                
        $formMapper->end();
        
        $formMapper->tab('Accueil');
        
            $formMapper->with('Interface ouverte', ['class' => 'col-md-8']);
               
                
                $formMapper->add('message_rappel_declaration', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,
                    'label'=>'Message de rappel (header)',
                    'attr' => ['placeholder' => '']]);
               /* $formMapper->add('message_rappel_introduction', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,  'label'=>'Message de rappel introduction (plus utilisé)','attr' => ['placeholder' => '']]);*/
                
                $formMapper->add('introduction_ouvert', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,  'label'=>'Introduction','attr' => ['placeholder' => '']]);
                $formMapper->add('introduction_ouvert_complement', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,  'label'=>'Complément d\'introduction ouvert (footer)','attr' => ['placeholder' => '']]);

            $formMapper->end();
            /*$formMapper->with('Message de rappel', ['class' => 'col-md-8']);
           
            $formMapper->end();*/
            $formMapper->with('Interface fermée', ['class' => 'col-md-8']);
            $formMapper->add('introduction_ferme', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Introduction fermé','attr' => ['placeholder' => '']]);
            $formMapper->end();
            
        $formMapper->end();
            
        
        
        /*$formMapper->tab('Coordonnées');
        
            $formMapper->with('Message de rappel', ['class' => 'col-md-8']);
                $formMapper->add('message_rappel_coordonnees', SimpleFormatterType::class, [
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default',
                    'required' => false,  'label'=>'Message de rappel coordonnées','attr' => ['placeholder' => '']]);
            $formMapper->end();
            
        $formMapper->end();
        */

        
        
        
        $formMapper->tab('Aide');
        

            
            $formMapper->with('Aide contextuelle', ['class' => 'col-md-8']);
                $formMapper->add('declaration_aide', KeyValueType::class,
                    array(
                        
                        'key_type'=> TextType::class,
                        'key_options'=>[
                            'label'=>'Titre',
                        ],
                        
                        'value_type' => SimpleFormatterType::class,
                        'value_options'=>[
                            'label'=>'Texte',
                            'ckeditor_context' => 'default',
                            'attr' => ['placeholder' => ''],
                            'format' => 'richhtml',
                            
                        ],
                        'label'=>'Utilisez le mot clef [activation_date] pour pointer sur la date calendaire ou scolaire configuré dans le contrat.'
                    ));
            $formMapper->end();
            $formMapper->end();
            
            
            $formMapper->tab('Validation');
            
            $formMapper->with('Message de validation', ['class' => 'col-md-8']);
            $formMapper->add('validation_confirm_message', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Message de validation de la déclaration','attr' => ['placeholder' => '']]);
            $formMapper->end();
            
            $formMapper->end();
            
            
            $formMapper->tab('Proforma');
            $formMapper->with('Message statut P', ['class' => 'col-md-8']);
            $formMapper->add('proformat_intro', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Message d\'introduction','help'=>"Message d'introduction de la page affiché en status P", 'attr' => ['placeholder' => '']]);
             
            $formMapper->end();
            $formMapper->with('Message statut W', ['class' => 'col-md-8']);
            $formMapper->add('proformat_confirmation', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Message de confirmation','help'=>"Message de confirmation de la page affiché en status W", 'attr' => ['placeholder' => '']]);
            
            $formMapper->end();
            $formMapper->end();

            
            
        //$formMapper->end();
        
        $formMapper->tab('Archives');
            $formMapper->with('Contenu introduction Historique', ['class' => 'col-md-8 abcdaire']);
             $formMapper->add('historique_intro_scolaire', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Introduction civile','attr' => ['placeholder' => '']]);
            $formMapper->add('historique_intro_civile', SimpleFormatterType::class, [
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'required' => false,  'label'=>'Introduction scolaire','attr' => ['placeholder' => '']])
            ->end();
            $formMapper->with('Configuration', ['class' => 'col-md-4 abcdaire'])
                ->add('archive', ChoiceType::class, ['choices' => $this->archive,'required' => true,   'label'=>'Type d\'archive', 'attr' => ['placeholder' => ''] ,'help'=>'Sans : N\'affiche aucuns liens archive ou derniere déclarations<br>Dernière déclaration : Affiche un lien pour consulter la derniere déclaration<br>Archive : Affiche un lien pour consulter les archives'])
                ->add('archive_ouverture', ChoiceType::class, ['choices' => $this->archive_ouverture,'required' => true,   'label'=>'Mode d\'affichage', 'attr' => ['placeholder' => ''] ,'help'=>''])
            ->end();
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('label');
        $datagridMapper->add('description');

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('_action', 'actions', [
            'header_style' => 'width: 120px',
            'actions' => [
                'edit' => [],
                'clone' => [
                    'template' => 'Admin/list__action_clone.html.twig'
                ]
            ]
        ]);
        

        $listMapper->addIdentifier('id', null, ['label' => 'id','editable' => false, 'header_style' => 'width: 50px']);
        $listMapper->addIdentifier('label', null, ['label' => 'type', 'editable' => true , 'header_style' => 'width: 150px']);
        $listMapper->add('departement', 'choice', ['label' => 'Département','header_style' => 'width: 350px', 'editable' => true, 'choices' => array_flip($this->departements), 'choices_as_values' => true ]);
        $listMapper->add('priorite', null, ['label' => 'Priorité','header_style' => 'width: 50px', 'editable' => true ]);
        $listMapper->add('type_interface', 'choice', ['label' => 'Type d\'interface','header_style' => 'width: 50px', 'editable' => true, 'choices' => array_flip($this->type_interface), 'choices_as_values' => true ]);
        
        $listMapper->add('description', null, ['label' => 'description','editable' => true ,'header_style' => '',]);
        $listMapper->add('label_interface', null, ['label' => 'Label de l\'interface', 'editable' => true , 'header_style' => 'width: 350px']);
        $listMapper->add('label_interface_home', null, ['label' => 'Label de l\'interface sur la home', 'editable' => true , 'header_style' => 'width: 350px']);
        $listMapper->add('actif', null, ['editable' => true,'header_style' => 'width: 50px']);
        
        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);
        
    }
    
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null): void
    {
        if (!$childAdmin && !\in_array($action, ['edit'], true)) {
            return;
        }
        
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $label=$this->hasSubject() && null !== $this->getSubject()->getLabel() ? $this->getSubject()->getLabel():null;
        
        $menu->addChild( $this->trans(' << Retour') ,[
                'uri' => '/admin/app/wtype/list'
            ]);
        
        $menu->addChild(
            sprintf($this->trans('Configuration du contrat %s') , $label),
            $admin->generateMenuUrl('edit', ['id' => $id])
           
            );
        
        $menu->addChild( sprintf($this->trans('Configuration des interfaces %s') , $label), 
            [
                'uri' => $admin->generateUrl('admin.wconf.list', ['id' => $id]) 
            ]);
        
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter().'/clone');
    }

}