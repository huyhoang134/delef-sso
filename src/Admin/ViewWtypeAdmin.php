<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sonata\CoreBundle\Form\Type\DatePickerType;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface as RoutingUrlGeneratorInterface;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

use Sonata\AdminBundle\Route\RouteCollection;
#use Pix\SortableBehaviorBundle\Services\PositionORMHandler as PositionHandler;
use Sonata\AdminBundle\Controller\CRUDController;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;


class ViewWtypeAdmin extends AbstractAdmin
{
    
    private $activation_dates=array(
        '01 janvier' => '01 janvier',
        '01 septembre' => '01 septembre',
    );
    
    
    private $departements=array(
        'Département entreprises et administrations' => 'DEA',
        'Département enseignement et formation' => 'DEF',
    );
    
    private $priorite=array(
        'Primaire' => '0',
        'Secondaire' => '1',
    );
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );
        

    }
    
    public function configure()
    {
        parent::configure();
    }
    

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('_action', 'actions', [
            'header_style' => 'width: 120px',
            'actions' => [
                'cocon' => [
                    'template' => 'Admin/list__action_cocon.html.twig'
                ]
            ]
        ]);
        

        $listMapper->add('label', null, ['label' => 'type', 'editable' => false , 'header_style' => 'width: 150px']);
        $listMapper->add('description', null, ['label' => 'description','editable' => false ,'header_style' => '',]);

        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);
    }
    
  
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null): void
    {
        
        if (!$childAdmin && !\in_array($action, ['edit'], true)) {
            return;
        }
        
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $label=$this->hasSubject() && null !== $this->getSubject()->getLabel() ? $this->getSubject()->getLabel():null;
        
        $menu->addChild( $this->trans(' << Retour à la liste des contrats') ,[
                'uri' => '/admin/app/viewwtype/list'
            ]);
        

        
        $menu->addChild( sprintf($this->trans('Co-contractants %s') , $label), 
            [
                'uri' => $admin->generateUrl('admin.wcocon.list', ['id' => $id]) 
            ]);
        
      
        if(!empty($admin->getRequest()->get('childId'))){

            $menu->addChild( sprintf($this->trans('Contrats du dossier %s') , $admin->getRequest()->get('childId')." : ". $admin->getChild('admin.wcocon')->getSubject()->getWcoRaisoc1(). " " .$admin->getChild('admin.wcocon')->getSubject()->getWcoRaisoc2()),
                [
                    'uri' => $admin->generateUrl('admin.view.wtype|admin.wcocon|admin.wcontrat.list', ['id' => $id, 'childId'=>$admin->getRequest()->get('childId')])
                ]);

            /*$menu->addChild( sprintf($this->trans('Déclarations du dossier %s') , $admin->getRequest()->get('childId')),
                [
                    'uri' => $admin->generateUrl('admin.view.wtype|admin.wcocon|admin.wdeclarlist.list', ['id' => $id, 'childId'=>$admin->getRequest()->get('childId')])
                ]);*/
        }


        return;
    }
    
    
    public function getExportFormats()
    {
        //remove the export button
        return [/*"json", "xml", "csv", "xls"*/];
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        
        
        //add route for our custuom button
        $collection->add('cocon', $this->getRouterIdParameter().'/wcocon/list');
        
        //$collection->remove('create');
        $collection->remove('edit');
        $collection->remove('delete');
        $collection->remove('show');
        $collection->remove('export');

    }
    
    public function configureActionButtons($action, $object = null)
    {
        $buttons = parent::configureActionButtons($action, $object);
        if (in_array($action, array('list'))) {
            unset($buttons['create']);
        }
    
        return $buttons;
    }
    
    //Remove batch actions
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);
        return $actions;
    }
    
    
}
