<?php

namespace App\Admin;

use App\Source\DBALStatementSourceIterator;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\Query\ResultSetMapping;

use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Sonata\AdminBundle\Route\RouteCollection;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
//use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sonata\MediaBundle\Provider\FileProvider;

/*
 * We don't need to edit declaration in admin.
 * Consider removing this class.
 * 
 * 
 * */
class WdeclarlistAdmin extends AbstractAdmin
{
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );

    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        //add route for our custuom button
        $collection->add('declar', $this->getRouterIdParameter().'/show');
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('delete');
        $collection->remove('export');/**/
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
    
        $listMapper->add('_action', 'actions', [
            'header_style' => 'width: 50px',
            'actions' => [
                 'declar' => [
                 'template' => 'Admin/list__action_declar.html.twig'
                ],
                
            ]
        ]);
    

        $listMapper->add('wde_declar', null, ['label' => 'Declaration', 'header_style' => 'width: 150px; text-align: center;','row_align' => 'center']);
        $listMapper->add('wde_contrat', null, ['label' => 'Contrat', 'header_style' => 'width: 150px; text-align: center;','row_align' => 'center']);
        $listMapper->add('wde_annee', null, ['label' => 'Année de déclar.', 'header_style' => 'width: 150px; text-align: center;','row_align' => 'center']);
        $listMapper->add('wde_etat_declar', null, ['label' => 'Statut', 'header_style' => 'width: 150px; text-align: center;','row_align' => 'center']);
        $listMapper->add('wde_email_sent', null, ['label' => 'Statut email facturation', 'header_style' => 'width: 150px; text-align: center;','row_align' => 'center']);
        
        $listMapper->add('wde_stamp', null, ['label' => 'Date de modification', 'header_style' => 'width: 200px; text-align: center;','row_align' => 'center']);
        $listMapper->add('wde_synchro', null, ['label' => 'Date de synchronisation', 'header_style' => 'text-align: left;']);
        
    
        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);
    
    }
    
    
    public function createQuery($context = 'list')
    {
        if($context=='list'){
    
            //Get current type id
            $typecontId=$this->getRequest()->get('id');
            $conconId=$this->getRequest()->get('childId');
            

            
            //Get current typecont
            $typecont=parent::createQuery()->getEntityManager()->getRepository('App\Entity\Wtype')->findOneBy(['id'=>$typecontId])->getLabel();
            
            $proxyQuery = parent::createQuery('list');
            $proxyQuery->leftJoin(
                'App\Entity\Wcocon',
                'c',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'c.wco_dossier = o.wde_dossier'
                );
            $proxyQuery->where('o.wde_typcont = :typecont');
            $proxyQuery->Andwhere('o.wde_dossier = :conconId');
            $proxyQuery->setParameter('typecont', $typecont);
            $proxyQuery->setParameter('conconId', $conconId);
             
        }
    
    
        return $proxyQuery;
    }
    
    
    
    public function get_declar_conf($Wdeclar){
         

        $admin = $this->isChild() ? $this->getParent() : $this;
        $wtype_id = $admin->getRequest()->get('id');
    
         
        $em = parent::createQuery()->getEntityManager();
        $WcoconRepository = $em->getRepository(Wcocon::class);
        $declaration_conf = $WcoconRepository->getDeclarationConfigurationByWdeclar($Wdeclar);

        $primary_declaration = $em->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $Wdeclar->getWdeDeclar()]);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);

        foreach($declaration_conf as $i=>$conf){
             
            foreach($conf as $j=>$c){
    
                if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                    $db_data_conf[$i]["wde_is_chorus_pro"]=$WcoconRepository->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_siret"]=$WcoconRepository->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_service_code"]=$WcoconRepository->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                }elseif($c->getTypeObjet()=="panorama_press_papier"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
    
                }elseif($c->getTypeObjet()=="file_upload")
                {
                    $data=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                    if(!empty($data)){
                        $mediaManager = $this->getConfigurationPool()->getContainer()->get('sonata.media.manager.media');
                        $media=$mediaManager->find($data);
                        $file_name=$media->getName();
    
                        $file_reference= $this->provider->getReferenceFile($media,'reference')->getName();
                        $media_key=md5($data.$WcoconRepository->download_key).'%'.$data;
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>$data,
                            'file_upload_fichier_name'=>$file_name,
                            'file_upload_fichier_reference'=>$file_reference,
                            'file_upload_fichier_media_key'=>$media_key,
                        ];
                    }else{
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>'',
                            'file_upload_fichier_name'=>"",
                            'file_upload_fichier_reference'=>"",
                            'file_upload_fichier_media_key'=>"",
                        ];
                    }
                }elseif($c->getTypeObjet()=="accordion_options")
                {
                    $TypeObjetConf=$c->getTypeObjetConf();
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar);
    
                }else{
                    if (!empty($c->getSynchroField())) {
                        $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                    }
                }
            }
        }
    
        return [
            'contrats'=>$contrats,
            'declaration_conf'=>$declaration_conf,
            'primary_declaration'=>$primary_declaration,
            'db_data_conf'=>$db_data_conf,
        ];
    }    
    
    
    
    
    
    
    
    
    public function getExportFormats()
    {
        //remove the export button
        return [/*"json", "xml", "csv", "xls"*/];
    }
    
    
    //Remove batch actions
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);
    
        return $actions;
    }

}