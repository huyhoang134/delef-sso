<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

final class WdeclarStatusAdmin extends AbstractAdmin
{
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
        ->add('wde_dossier')
        ->add('wde_contrat')
        ->add('wde_declar')
        ->add('wde_etat_declar')
        ->add('wde_typcont')
        

            
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
        ->add('wde_contrat')
        ->add('wde_dossier', null, ['editable' => true])
        ->add('wde_declar', null, ['editable' => true])
        ->add('wde_etat_declar', null, ['editable' => true])
        ->add('wde_typcont', null, ['editable' => true])
        ->add('wde_email_sent', null, ['editable' => true])
        
        ->add('wde_annee', null, ['editable' => true])
        ->add('wde_order_number', null, ['editable' => true])
        ;
        /*
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);*/
            
            unset($this->listModes['mosaic']);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('wde_stamp')
            ->add('wde_synchro')
            ->add('wde_declar')
            ->add('wde_contrat')
            ->add('wde_annee')
            ->add('wde_eleves')
            ->add('wde_tranche')
            ->add('wde_copieurs')
            ->add('wde_profs')
            ->add('wde_etat_declar')
            ->add('wde_date_validation')
            ->add('wde_email_sent')
            ->add('wde_nom_resp')
            ->add('wde_tit_resp')
            ->add('wde_fct_resp')
            ->add('wde_mel_resp')
            ->add('wde_effectif')
            ->add('wde_tranche01')
            ->add('wde_tranche02')
            ->add('wde_tranche03')
            ->add('wde_tranche04')
            ->add('wde_tranche05')
            ->add('wde_tranche06')
            ->add('wde_tranche07')
            ->add('wde_nom_sec_gen')
            ->add('wde_tit_sec_gen')
            ->add('wde_fct_sec_gen')
            ->add('wde_mel_sec_gen')
            ->add('wde_annee_decl')
            ->add('wde_tranche08')
            ->add('wde_tranche09')
            ->add('wde_tranche10')
            ->add('wde_tranche11')
            ->add('wde_tranche12')
            ->add('wde_tranche13')
            ->add('wde_tranche14')
            ->add('wde_tranche15')
            ->add('wde_tranche16')
            ->add('wde_tranche17')
            ->add('wde_tranche18')
            ->add('wde_tranche19')
            ->add('wde_tranche20')
            ->add('wde_tranche21')
            ->add('wde_tranche22')
            ->add('wde_tel_resp')
            ->add('wde_fax_resp')
            ->add('wde_effectif2')
            ->add('wde_effectif3')
            ->add('wde_effectif4')
            ->add('wde_is_chorus_pro')
            ->add('wde_siret')
            ->add('wde_service_code')
            ->add('wde_is_order_number')
            ->add('wde_order_number')
            ->add('wde_typcont')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('wde_stamp')
            ->add('wde_synchro')
            ->add('wde_declar')
            ->add('wde_contrat')
            ->add('wde_annee')
            ->add('wde_eleves')
            ->add('wde_tranche')
            ->add('wde_copieurs')
            ->add('wde_profs')
            ->add('wde_etat_declar')
            ->add('wde_date_validation')
            ->add('wde_email_sent')
            ->add('wde_nom_resp')
            ->add('wde_tit_resp')
            ->add('wde_fct_resp')
            ->add('wde_mel_resp')
            ->add('wde_effectif')
            ->add('wde_tranche01')
            ->add('wde_tranche02')
            ->add('wde_tranche03')
            ->add('wde_tranche04')
            ->add('wde_tranche05')
            ->add('wde_tranche06')
            ->add('wde_tranche07')
            ->add('wde_nom_sec_gen')
            ->add('wde_tit_sec_gen')
            ->add('wde_fct_sec_gen')
            ->add('wde_mel_sec_gen')
            ->add('wde_annee_decl')
            ->add('wde_tranche08')
            ->add('wde_tranche09')
            ->add('wde_tranche10')
            ->add('wde_tranche11')
            ->add('wde_tranche12')
            ->add('wde_tranche13')
            ->add('wde_tranche14')
            ->add('wde_tranche15')
            ->add('wde_tranche16')
            ->add('wde_tranche17')
            ->add('wde_tranche18')
            ->add('wde_tranche19')
            ->add('wde_tranche20')
            ->add('wde_tranche21')
            ->add('wde_tranche22')
            ->add('wde_tel_resp')
            ->add('wde_fax_resp')
            ->add('wde_effectif2')
            ->add('wde_effectif3')
            ->add('wde_effectif4')
            ->add('wde_is_chorus_pro')
            ->add('wde_siret')
            ->add('wde_service_code')
            ->add('wde_is_order_number')
            ->add('wde_order_number')
            ->add('wde_typcont')
            ;
    }
}
