<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class WdelefAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('wdfDeclar')
            ->add('wdfStamp')
            ->add('wdfSynchro')
            ->add('wdfDossier')
            ->add('wdfContrat')
            ->add('wdfAnnee')
            ->add('wdfAnneeDecl')
            ->add('wdfEleves')
            ->add('wdfTranche')
            ->add('wdfCopieurs')
            ->add('wdfProfs')
            ->add('wdfEtatDeclar')
            ->add('wdfDateValidation')
            ->add('wdfEmailSent')
            ->add('wdfEffectif')
            ->add('wdfTranche01')
            ->add('wdfTranche02')
            ->add('wdfTranche03')
            ->add('wdfTranche04')
            ->add('wdfTranche05')
            ->add('wdfTranche06')
            ->add('wdfTranche07')
            ->add('wdfIntituleDecl')
            ->add('wdfTranche08')
            ->add('wdfTranche09')
            ->add('wdfTranche10')
            ->add('wdfTranche11')
            ->add('wdfTranche12')
            ->add('wdfTranche13')
            ->add('wdfTranche14')
            ->add('wdfTranche15')
            ->add('wdfTranche16')
            ->add('wdfEffectif2')
            ->add('wdfEffectif3')
            ->add('libelletarif')
            ->add('fkproforma')
            ->add('wdfEffectif4')
            ->add('wdfTranche17')
            ->add('wdfTranche18')
            ->add('wdfTranche19')
            ->add('wdfTranche20')
            ->add('wdfTranche21')
            ->add('wdfTranche22')
            ->add('motifattente')
            ->add('wdfNumperiod')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('wdfDeclar')
            ->add('wdfStamp')
            ->add('wdfSynchro')
            ->add('wdfDossier')
            ->add('wdfContrat')
            ->add('wdfAnnee')
            ->add('wdfAnneeDecl')
            ->add('wdfEleves')
            ->add('wdfTranche')
            ->add('wdfCopieurs')
            ->add('wdfProfs')
            ->add('wdfEtatDeclar')
            ->add('wdfDateValidation')
            ->add('wdfEmailSent')
            ->add('wdfEffectif')
            ->add('wdfTranche01')
            ->add('wdfTranche02')
            ->add('wdfTranche03')
            ->add('wdfTranche04')
            ->add('wdfTranche05')
            ->add('wdfTranche06')
            ->add('wdfTranche07')
            ->add('wdfIntituleDecl')
            ->add('wdfTranche08')
            ->add('wdfTranche09')
            ->add('wdfTranche10')
            ->add('wdfTranche11')
            ->add('wdfTranche12')
            ->add('wdfTranche13')
            ->add('wdfTranche14')
            ->add('wdfTranche15')
            ->add('wdfTranche16')
            ->add('wdfEffectif2')
            ->add('wdfEffectif3')
            ->add('libelletarif')
            ->add('fkproforma')
            ->add('wdfEffectif4')
            ->add('wdfTranche17')
            ->add('wdfTranche18')
            ->add('wdfTranche19')
            ->add('wdfTranche20')
            ->add('wdfTranche21')
            ->add('wdfTranche22')
            ->add('motifattente')
            ->add('wdfNumperiod')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('wdfDeclar')
            ->add('wdfStamp')
            ->add('wdfSynchro')
            ->add('wdfDossier')
            ->add('wdfContrat')
            ->add('wdfAnnee')
            ->add('wdfAnneeDecl')
            ->add('wdfEleves')
            ->add('wdfTranche')
            ->add('wdfCopieurs')
            ->add('wdfProfs')
            ->add('wdfEtatDeclar')
            ->add('wdfDateValidation')
            ->add('wdfEmailSent')
            ->add('wdfEffectif')
            ->add('wdfTranche01')
            ->add('wdfTranche02')
            ->add('wdfTranche03')
            ->add('wdfTranche04')
            ->add('wdfTranche05')
            ->add('wdfTranche06')
            ->add('wdfTranche07')
            ->add('wdfIntituleDecl')
            ->add('wdfTranche08')
            ->add('wdfTranche09')
            ->add('wdfTranche10')
            ->add('wdfTranche11')
            ->add('wdfTranche12')
            ->add('wdfTranche13')
            ->add('wdfTranche14')
            ->add('wdfTranche15')
            ->add('wdfTranche16')
            ->add('wdfEffectif2')
            ->add('wdfEffectif3')
            ->add('libelletarif')
            ->add('fkproforma')
            ->add('wdfEffectif4')
            ->add('wdfTranche17')
            ->add('wdfTranche18')
            ->add('wdfTranche19')
            ->add('wdfTranche20')
            ->add('wdfTranche21')
            ->add('wdfTranche22')
            ->add('motifattente')
            ->add('wdfNumperiod')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('wdfDeclar')
            ->add('wdfStamp')
            ->add('wdfSynchro')
            ->add('wdfDossier')
            ->add('wdfContrat')
            ->add('wdfAnnee')
            ->add('wdfAnneeDecl')
            ->add('wdfEleves')
            ->add('wdfTranche')
            ->add('wdfCopieurs')
            ->add('wdfProfs')
            ->add('wdfEtatDeclar')
            ->add('wdfDateValidation')
            ->add('wdfEmailSent')
            ->add('wdfEffectif')
            ->add('wdfTranche01')
            ->add('wdfTranche02')
            ->add('wdfTranche03')
            ->add('wdfTranche04')
            ->add('wdfTranche05')
            ->add('wdfTranche06')
            ->add('wdfTranche07')
            ->add('wdfIntituleDecl')
            ->add('wdfTranche08')
            ->add('wdfTranche09')
            ->add('wdfTranche10')
            ->add('wdfTranche11')
            ->add('wdfTranche12')
            ->add('wdfTranche13')
            ->add('wdfTranche14')
            ->add('wdfTranche15')
            ->add('wdfTranche16')
            ->add('wdfEffectif2')
            ->add('wdfEffectif3')
            ->add('libelletarif')
            ->add('fkproforma')
            ->add('wdfEffectif4')
            ->add('wdfTranche17')
            ->add('wdfTranche18')
            ->add('wdfTranche19')
            ->add('wdfTranche20')
            ->add('wdfTranche21')
            ->add('wdfTranche22')
            ->add('motifattente')
            ->add('wdfNumperiod')
            ;
    }
}
