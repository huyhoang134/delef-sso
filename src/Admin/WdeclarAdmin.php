<?php

namespace App\Admin;

use App\Source\DBALStatementSourceIterator;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\Query\ResultSetMapping;

/*
 * We don't need to edit declaration in admin.
 * Consider removing this class.
 * 
 * 
 * */
class WdeclarAdmin extends AbstractAdmin
{
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );

    }
    
    
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->tab('General');
            $formMapper->with('Informations générales', ['class' => 'col-md-8']);
                //$formMapper->add('wde_stamp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_declar', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);

                $formMapper->add('wde_dossier', EntityType::class, [
                    'class' => 'App\Entity\Wcocon',
                    'choice_label' => 'wco_dossier',
                    'label' => 'wde_dossier',
                    'multiple' => false,
                    'required' => false,
                ]);
                
                //$formMapper->add('wde_synchro', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_contrat', IntegerType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_annee', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                
             /* $formMapper->add('wde_typcont', EntityType::class, [
                    'class' => 'App\Entity\Wtype',
                    'choice_label' => 'label',
                    'label' => 'Type de contrat',
                    'multiple' => false,
                    'required' => false,
                ]);  */
                
                $em=$this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('label', 'label');
                $query = $em->createNativeQuery('SELECT label FROM `wtype`', $rsm);
                $results = $query->getResult();
                $choices=array();
                foreach($results as $r){
                    $choices[$r['label']]=$r['label'];
                }

                $formMapper->add('wde_typcont', ChoiceType::class, ['choices' => $choices]);
                
                    
                    
               /* $formMapper->add('wde_typcont', EntityType::class, [
                    'class' => 'App\Entity\Wtype2',
                    'choice_label' => function ($Wtype) {
                        return $Wtype->getLabel();
                        }
                    ]);
                */
                
                $formMapper->add('wde_eleves', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_copieurs', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_profs', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_etat_declar', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_date_validation', DateTimeType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_email_sent', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_nom_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tit_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_fct_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_mel_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_effectif', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche01', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche02', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche03', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche04', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche05', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche06', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche07', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_nom_sec_gen', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tit_sec_gen', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_fct_sec_gen', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_mel_sec_gen', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_annee_decl', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche08', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche09', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche10', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche11', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche12', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche13', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche14', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche15', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche16', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche17', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche18', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche19', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche20', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche21', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tranche22', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_tel_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_fax_resp', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_effectif2', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_effectif3', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_effectif4', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_effectif4', TextType::class, ['required' => false,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_is_chorus_pro', IntegerType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_siret', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_service_code', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_is_order_number', IntegerType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
                $formMapper->add('wde_order_number', TextType::class, ['required' => true,  'label'=>'','attr' => ['placeholder' => '']]);
            $formMapper->end();
        $formMapper->end();
            
     
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('wde_declar');
        $datagridMapper->add('wde_dossier');
        $datagridMapper->add('wde_contrat');
        $datagridMapper->add('wde_annee');
        $datagridMapper->add('wde_typcont', null, ['show_filter' => true , 'label' => 'Type de contrat']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('wde_declar', null, ['label' => 'wde_declar']);
        $listMapper->addIdentifier('wde_dossier', null, ['label' => 'wde_dossier']);
        $listMapper->addIdentifier('wde_contrat', null, ['label' => 'wde_contrat']);
        $listMapper->addIdentifier('wde_annee', null, ['label' => 'wde_annee']);
        $listMapper->addIdentifier('wde_typcont', null, ['label' => 'wde_typcont']);

        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);
    }
    
    public function prePersist($object)
    {
        $this->preUpdate($object);
    }
    
    public function preUpdate($object)
    {

    }

    public function getDataSourceIterator()
    {
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $conn = $em->getConnection();
        $fields = $this->getExportFields();
        $field_str = implode(',', $fields);
        $sql = "SELECT {$field_str} FROM wdeclar d LEFT JOIN wcompos c ON c.wcp_declar = d.wde_declar WHERE d.wde_typcont LIKE '%FP / MEN%'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return new DBALStatementSourceIterator($stmt);
    }

    public function getExportFields() {
        return [
            'd.wde_stamp','d.wde_synchro','d.wde_declar','d.wde_dossier','d.wde_contrat','d.wde_annee','d.wde_typcont','d.wde_eleves',
            'd.wde_tranche','d.wde_copieurs','d.wde_profs','d.wde_etat_declar',
            'd.wde_date_validation','d.wde_email_sent','d.wde_nom_resp','d.wde_tit_resp','d.wde_fct_resp',
            'd.wde_mel_resp','d.wde_effectif','d.wde_tranche01','d.wde_tranche02','d.wde_tranche03','d.wde_tranche04','d.wde_tranche05',
            'd.wde_tranche06','d.wde_tranche07','d.wde_nom_sec_gen','d.wde_tit_sec_gen','d.wde_fct_sec_gen','d.wde_mel_sec_gen',
            'd.wde_annee_decl','d.wde_tranche08','d.wde_tranche09','d.wde_tranche10','d.wde_tranche11','d.wde_tranche12',
            'd.wde_tranche13','d.wde_tranche14','d.wde_tranche15','d.wde_tranche16','d.wde_tranche17','d.wde_tranche18','d.wde_tranche19','d.wde_tranche20','d.wde_tranche21','d.wde_tranche22','d.wde_tel_resp','d.wde_fax_resp',
            'd.wde_effectif2','d.wde_effectif3','d.wde_is_chorus_pro','d.wde_siret','d.wde_service_code','d.wde_is_order_number', 'd.wde_order_number',
            'c.wcp_stamp','c.wcp_synchro','c.wcp_declar','c.wcp_libelle','c.wcp_effectif','c.wcp_heures',
        ];
    }

}