<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManagerInterface;

class AppExtension extends AbstractExtension
{

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em        = $em;
    }


    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
            new TwigFilter('filtreCrochet', [$this, 'filtreCrochet']),
            new TwigFilter('basename', [$this, 'basename']),
            new TwigFilter('getTypeCont', [$this, 'getTypeCont']),
            new TwigFilter('getTypeContDelpe', [$this, 'getTypeContDelpe']),
        ];
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function filtreCrochet($string)
    {
        $expl=explode('[', $string);
        if(count($expl)){
            return $expl[0];
        }else{
            return $string;
        }


    }

    public function getTypeCont($typeCont){
        $em  = $this->container->get('doctrine')->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('label', 'label');
        $rsm->addScalarResult('description', 'description');
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('type_interface', 'type_interface');
        $query = $em->createNativeQuery('SELECT id, label, description, type_interface FROM `wtype` where label=? ', $rsm);
        $query->setParameter(1, $typeCont);
        $results = $query->getResult();

        return $results[0];
    }

    public function getTypeContDelpe($typeCont){

        $em  = $this->container->get('doctrine')->getManager("delpe");
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('label', 'label');
        $rsm->addScalarResult('description', 'description');
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('type_interface', 'type_interface');
        $query = $em->createNativeQuery('SELECT id, label, description, type_interface FROM `wtype` where label=? ', $rsm);
        $query->setParameter(1, $typeCont);
        $results = $query->getResult();

        return $results[0];
    }
    public function basename($string)
    {
        return basename($string);
    }
}

?>
