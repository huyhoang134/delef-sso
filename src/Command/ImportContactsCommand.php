<?php 
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Filesystem\Filesystem;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportContactsCommand extends ContainerAwareCommand
{
    private $container;
    private $nb_thread;
    private $my_argument_name;
    
    protected function configure()
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('delef:import:contacts')
        
        // the short description shown while running "php bin/console list"
        ->setDescription('Import Contacts database')
        
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('');
        
        //add an argument to the task
        //$this->addArgument('extension', InputArgument::REQUIRED, 'my_argument_name');
        
    }
    
    public function __construct(ContainerInterface $container)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        //$this->requirePassword = $requirePassword;
    
        parent::__construct();
        $this->container = $container;
        
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cn = $this->container->get('doctrine')->getManager()->getConnection();
        $stmt = $cn->prepare("TRUNCATE `wContact`");
        $stmt->execute();
        
        
        $io = new SymfonyStyle($input, $output);
        
        
        //$this->my_argument_name = $input->getArgument('my_argument_name');
        $io->title('Import des données Contacs');
        $ExcellFile="./public/import/DelefContact.xls";
        $io->text('Fichier à importer :'.$ExcellFile);
        
        
        
        
        $objReader = IOFactory::createReader('Xls');
        
        $objPHPExcel = $objReader->load($ExcellFile);
        $sheet=$objPHPExcel->setActiveSheetIndex(0);
        
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        
        $columnLoopLimiter = $highestColumn;
        ++$columnLoopLimiter;
 
        $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE, TRUE)[1];
        
        
        $io->text('Nombre d\'enregistrements : '.$highestRow);
        $io->section('Insertion en base');
        $io->progressStart($highestRow);
        
        for ($row = 1; $row <= $highestRow; $row++)
        {
           $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE, TRUE);
           $sql="INSERT INTO `wContact` (`PkContact`, `DateSQL`, `NomPrenom`, `Titre`, `TitreMailing`, `Fonction`, `Service`, `Tel1`, `Tel2`, `Fax`, `Mobile`, `Email`, `FkSiteGeo`, `FkCocontractant`, `Actif`) VALUES 
               (
               :PkContact, 
               :DateSQL, 
               :NomPrenom, 
               :Titre, 
               :TitreMailing, 
               :Fonction, 
               :Service, 
               :Tel1, 
               :Tel2, 
               :Fax, 
               :Mobile, 
               :Email, 
               :FkSiteGeo, 
               :FkCocontractant, 
               :Actif
               );";
           $stmt = $cn->prepare($sql);
           
           $stmt->execute(array(
               'PkContact'=>$rowData[$row]['A'], 
               'DateSQL'=>$rowData[$row]['B'],  
               'NomPrenom'=>$rowData[$row]['C'],  
               'Titre'=>$rowData[$row]['D'],  
               'TitreMailing'=>$rowData[$row]['E'],  
               'Fonction'=>$rowData[$row]['F'],  
               'Service'=>$rowData[$row]['G'],  
               'Tel1'=>$rowData[$row]['H'],  
               'Tel2'=>$rowData[$row]['I'],  
               'Fax'=>$rowData[$row]['J'],  
               'Mobile'=>$rowData[$row]['K'],  
               'Email'=>$rowData[$row]['L'],  
               'FkSiteGeo'=>$rowData[$row]['M'],  
               'FkCocontractant'=>$rowData[$row]['N'],  
               'Actif'=>$rowData[$row]['O'], 
               
            ));   
               
           $io->progressAdvance();
        }
        
        $io->progressFinish();
              
    }
}
?>