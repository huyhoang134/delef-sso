<?php 
namespace App\Command;

use App\Entity\Wconf;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;




class StoreOneDeclarationCommand extends Command
{
    private $nb_thread;
    private $my_argument_name;

    /**
     *  @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var Environment
     */
    protected $twig;
    /** 
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(ManagerRegistry $doctrine, Environment $twig, \Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $this->doctrine->getManager();
        $this->params = $params;
    }

    protected function configure()
    {
        $this->setName('delef:store-one-declar')
            ->setDescription('Store declaration in ./tmp_store folder')
            ->setHelp('')
            ->addArgument('contrat_id', InputArgument::REQUIRED, 'wdeclar id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->property_accessor = PropertyAccess::createPropertyAccessor();
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');
        
        $contrat_id=$input->getArgument('contrat_id');
        

        $io = new SymfonyStyle($input, $output);
        
        //$io->title('Delef Email Validation');
        
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        /** @var Wdeclar[] */
        $qb=$this->em->createQueryBuilder();

        $qb->select('d')->from('App\Entity\Wcontrat', 'c')
            ->leftJoin(
                'App\Entity\Wtype',
                't',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'c.wct_typcont = t.label'
            )->leftJoin(
                'App\Entity\Wdelef',
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.wdfContrat = c.wct_contrat'
            )
            //->where("d.wde_declar = 5830 ")
            ->where("c.wct_contrat = ".$contrat_id." ");

        $declars=$qb->getQuery()->getResult();

        foreach ($declars as $i=>$wdelef) {

            $wcocon = $wdelef->getWdfDossier();
            $wtype = $this->doctrine
                ->getRepository(Wtype::class)
                ->findOneBy(['label' => $wdelef->getWdfContrat()->getWctTypcont()])
            ;

            $WcoconRepository=$this->doctrine->getRepository(Wcocon::class);
            $declaration_conf=$WcoconRepository->getDeclarationConfigurationByWcontrat($wdelef->getWdfContrat());
                
            $WcoconRepository = $this->doctrine->getRepository(Wcocon::class);
            //$pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar(), true);


            if (!file_exists('./tmp_store/')) {
                mkdir('./tmp_store', 0777, true);
            }
            $folder='./tmp_store/'.$wdelef->getWdfDossier().'_'.$wdelef->getWdfContrat()->getWctContrat();
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }
            $_SERVER['HTTP_HOST']='espace-client.cfcopies.com';

                //$pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar(), true);
                $pdf_file_path = $WcoconRepository->GenerateExcell($wdelef->getWdfContrat()->getWctContrat());
                copy($pdf_file_path['path'], $folder.'/'.$wdelef->getWdfDossier().'_'.date('Y-m-d').'.xlsx');

                
                //place validation proccessing declaration here. Change status of declaratio + send email confirmation
                //copier collé de DeclarationController.php ligne 309
                foreach($declaration_conf as $i=>$conf){
                    foreach($conf as $j=>$c){
                        if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                            $db_data_conf[$i]["wde_is_chorus_pro"]=$WcoconRepository->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i]["wde_siret"]=$WcoconRepository->getValue("wde_siret", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i]["wde_service_code"]=$WcoconRepository->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        }elseif($c->getTypeObjet()=='bon_commande'){
                            $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                            $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        }elseif($c->getTypeObjet()=="panorama_press_papier"){
                            $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        }elseif($c->getTypeObjet()=="file_upload")
                        {
                            $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            if(!empty($db_data_conf[$i][$c->getSynchroField()])){
                                $mediaManager = $container->get('sonata.media.manager.media');
                                $media=$mediaManager->find($db_data_conf[$i][$c->getSynchroField()]);
                                $file_name=$media->getName();
                                $file_reference=$provider->getReferenceFile($media,'reference')->getName();
                                $media_key=md5($db_data_conf[$i][$c->getSynchroField()].$WcoconRepository->download_key).'%'.$db_data_conf[$i][$c->getSynchroField()];
                                $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                                    'file_upload_id'=>$db_data_conf[$i][$c->getSynchroField()],
                                    'file_upload_fichier_name'=>$file_name,
                                    'file_upload_fichier_reference'=>$file_reference,
                                    'file_upload_fichier_media_key'=>$media_key,
                                ];
                            }else{
                                $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                                    'file_upload_id'=>'',
                                    'file_upload_fichier_name'=>"",
                                    'file_upload_fichier_reference'=>"",
                                    'file_upload_fichier_media_key'=>"",
                                ];
                            }
                        }elseif($c->getTypeObjet()=="accordion_options")
                        {
                            $TypeObjetConf=$c->getTypeObjetConf();
                            $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                        }else{
                            if (!empty($c->getSynchroField())) {
                                $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar, $c->wdf_declar);
                            }
                        }
                    }
                }
                
                
                foreach($db_data_conf as $conf){
                    
                     if(!empty($conf['panorama_press'])){
                         foreach($conf['panorama_press']["wpanoramapress_liste"] as $pano){
                             if(!empty($pano['wppl_fichier'])){
                                 copy('./public/upload/media/'.$pano['wppl_fichier_media_reference'], $folder.'/'.$pano['wppl_fichier_name']);
                             }
                         }
                     }
                    
                    
                    /* if(!empty($conf['panorama_press'])){
                         foreach($conf['panorama_press']["wpanoramapress_liste"] as $pano){
                             if(!empty($pano['wppl_fichier'])){
                                 $filename=$pano['wppl_fichier_name'];
                                 $projectRoot = $kernel->getProjectDir();
                                 $message->attach(
                                     \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$pano['wppl_fichier_media_reference'])->setFilename($filename)
                                     );
                             }
                         }
                     }*/
                     if(!empty($conf['abo_press_livres'])){
                         if(!empty($conf['abo_press_livres']['wabo_fichier'])){
                             copy('./public/upload/media/'.$conf['abo_press_livres']['wabo_fichier_media_reference'], $folder.'/'.$conf['abo_press_livres']['wabo_fichier_name']);
                         }
                     }
                
                     if(!empty($conf["file_upload"])){
                         foreach($conf["file_upload"] as $file)
                         {
                             copy('./public/upload/media/'.$file['file_upload_fichier_reference'], $folder.'/'.$file['file_upload_fichier_name']);
                             
                         }
                     }
                  }
             //$io->note(memory_get_usage() .' - fichier pdf : '.$pdf_file_path['path']);
             //echo memory_get_usage() .' - fichier pdf : '.$pdf_file_path['path']."\n";

        }


    }
    
    private function getEmailValue($declar, $wcocon, $field_name){
        
        if (substr($field_name, 0, 3) == 'wde') {
            $target_email = $this->property_accessor->getValue($declar, $field_name);
        }
        else if (substr($field_name, 0, 3) == 'wco') {
            $target_email = $this->property_accessor->getValue($wcocon, $field_name);
        }
        return str_replace('..','.',$target_email);
    }
    

}
