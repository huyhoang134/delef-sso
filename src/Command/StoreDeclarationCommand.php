<?php 
namespace App\Command;

use App\Entity\Wconf;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;




class StoreDeclarationCommand extends Command
{
    private $nb_thread;
    private $my_argument_name;

    /**
     *  @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var Environment
     */
    protected $twig;
    /** 
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(ManagerRegistry $doctrine, Environment $twig, \Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $this->doctrine->getManager();
        $this->params = $params;
    }

    protected function configure()
    {
        $this->setName('delef:store-declar')
            ->setDescription('Store declaration (bien vérifier la requete sql de séléction)')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->property_accessor = PropertyAccess::createPropertyAccessor();
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');

        $io = new SymfonyStyle($input, $output);
        
        $io->title('Delef Email Validation');
        
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        /** @var Wdeclar[] */
        $qb=$this->em->createQueryBuilder();
        $qb->select('d')->from('App\Entity\Wcontrat', 'c')
        ->leftJoin(
            'App\Entity\Wtype',
            't',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'c.wct_typcont = t.label'
            )->leftJoin(
                'App\Entity\Wdelef',
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.wdfContrat = c.wct_contrat'
            )
        //->where("d.wde_declar = 5830 ")
        ->where("c.wct_typcont in ('ENS / SAN 1', 'CNI/ST / DEA') 
            and d.wdf_date_validation <= '2023-03-02 08:00:00' 
            and d.wdf_etat_declar='V'
            order by d.wdfContrat asc
            ")
            /*
         SELECT * FROM `wdeclar`  as d where d.wde_typcont in ('CIP / DEA', 'CNI/ST / DEA') 
            and d.wde_date_validation <= '2022-03-03 08:00:00' 
            and d.wde_etat_declar='V'
            order by d.wde_contrat asc
             * 
             * */
        //->AndWhere('d.wde_email_sent = :wde_email_sent')
        //->AndWhere('t.priorite = :priorite')
       // ->setParameter('wde_etat_declar', 'F')
        //->setParameter('wde_email_sent', 0);
        //->groupBy('d.wde_dossier');
        //->setParameter('priorite', 0)
        ;
        $declars=$qb->getQuery()->getResult();
        foreach ($declars as $i=>$declar) {
            
            echo 'bin/console delef:store-one-declar '.$declar->getWdfContrat()->getWctContrat()."\n";
            exec('bin/console delef:store-one-declar '.$declar->getWdfContrat()->getWctContrat(), $output, $retval);
            echo @$output[0]."\n";
            $output=null;
            
        }


    }
    
    private function getEmailValue($declar, $wcocon, $field_name){
        
        if (substr($field_name, 0, 3) == 'wde') {
            $target_email = $this->property_accessor->getValue($declar, $field_name);
        }
        else if (substr($field_name, 0, 3) == 'wco') {
            $target_email = $this->property_accessor->getValue($wcocon, $field_name);
        }
        return str_replace('..','.',$target_email);
    }
    

}
