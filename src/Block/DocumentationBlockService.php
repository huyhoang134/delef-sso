<?php 
// src/AdminBundle/Block 
namespace App\Block; 
 
use Sonata\BlockBundle\Block\BlockContextInterface; 
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface; 
use Sonata\AdminBundle\Form\FormMapper; 

use Sonata\AdminBundle\Admin\Pool; 

use Sonata\BlockBundle\Block\BaseBlockService; 
use Symfony\Component\Security\Core\SecurityContext; 
use Symfony\Component\OptionsResolver\OptionsResolverInterface; 
use Doctrine\ORM\EntityManager; 
 
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Validator\ErrorElement;


class DocumentationBlockService extends BaseBlockService 
{ 

 
    /** * @var EntityManager */
    protected $em; 
 
    public function __construct(
      $name, 
      EngineInterface $templating, 
      Pool $pool, 
      EntityManager $em
      ) { 
        parent::__construct($name, $templating); 
        $this->pool = $pool;
        $this->em = $em;

    }
 
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Statistique';
    }
 
 
    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        
        
 
        // Votre code métier...
 
        // merge settings
        $settings = array_merge($this->getDefaultSettings(), $blockContext->getSettings());
 
        return $this->renderResponse($blockContext->getTemplate(), array(
            'block'         => $blockContext->getBlock(),
            'base_template' => $this->pool->getTemplate('layout'),         
            'settings'      => $blockContext->getSettings()
        ), $response);
    }
    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'title'    => 'Mes informations',
            'template' => 'Block/Documentation.html.twig' // Le template à render dans execute()
        ));
    }
 
    public function getDefaultSettings()
    {
        return array();
    }
 
    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {}
 
    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block) {}
}