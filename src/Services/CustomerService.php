<?php

namespace App\Services;

use App\Entity\Wcontrat;
use App\Entity\Wdeclar;
use App\Entity\Wcocon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomerService 
{
    /**
     * @var SSOApi
     */
    private $ssoApi;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    public function __construct(EntityManagerInterface $entityManager, SSOApi $ssoApi, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->ssoApi = $ssoApi;
        $this->params = $params;
    }

    public function loginCheckAction(array $request): JsonResponse
    {
        $url  = '/api/v1/security/login-grant';

        $postFields = [
            'username' => $request['_username'],
            'password' => $request['_password'],
        ];

        [$res, $code] = $this->ssoApi->request($url, true, $postFields);
        

        if ($res['status'] !== 'ok') {
            return new JsonResponse([$res['message']]);
        }


        $user=$this->logUser($res['data']['user'], $res['data']['tokens']);

        if (empty($user)) {
            return new JsonResponse(['Utilisateur inconu']);
        }

        return new JsonResponse([
            'token'         => $res['data']['tokens']['access_token']['hash'],
            'name'          => $res['data']['user'],
        ]);
    }
    
    public function getUserDetail()
    {
        $token      = $_GET['backToken'] ;//request->get('backUsername', null);
        $url  = '/api/v1/security/get-user-by-token?SaveToken='.urlencode($token);

        [$res, $code] = $this->ssoApi->request($url,false);

        if ($res['status'] !== 'ok') {
            return new JsonResponse($res['message']);
        }

        $this->logUser($res['data']['user'], $res['data']['tokens']);

        return $this->syncUser($res['data']['user']);

    }

    /**
     * Log user.
     *
     * @param array $userData       The user data
     * @param array $token          The token
     *
     * @return object
     */
    public function logUser(array $userData, array $token)
    {
        $_SESSION['tokens'] = [
            'token'         => $token['access_token']['hash'],
        ];
        $_SESSION['user_data'] = [
            'token'         => $userData,
        ];
        return $this->syncUser($userData);
    }

    public function syncUser($userData)
    {
        $wcontrats_list=[];
        $sso_data_table=$this->params->get('sso_data_table');

        foreach($userData as $u){

            if(is_array($u)){
                if($u['sso_table']=="wcontrat"){
                    $wcontrat_id=$u['sso_id_reccord'];

                    $wcontrats_list[]=$this->entityManager->getRepository(Wcontrat::class)->findOneBy([
                        'wct_contrat' => $wcontrat_id,
                    ]);
                }
                if($u['sso_table']=="delpe"){
                    $delpe_id=$u['sso_id_reccord'];

                }
            }elseif(is_object($u)){
                if($u->sso_table=="wcontrat"){
                    $wcontrat_id=$u->sso_id_reccord;
                    $wcontrats_list[]=$this->entityManager->getRepository(Wcontrat::class)->findOneBy([
                        'wct_contrat' => $wcontrat_id,
                    ]);
                }
                if($u->sso_table=="delpe"){
                    $delpe_id=$u->sso_id_reccord;
                }
            }
        }

        //Si l'utilisateur a uniquement DELPE sans wdeclar. ex contrats PRIMAIRES.
        if(empty($wcontrats_list) && !empty($delpe_id))
        {
            $user= new Wcocon();
            $user->setDelpeId($delpe_id);
            $user->setSsoSyncData($userData);
            return $user;
        }

        $user=$this->entityManager->getRepository(Wcocon::class)->findOneBy([
            'wco_dossier' => $wcontrats_list[0]->getWctDossier(),
        ]);

        if(!empty($delpe_id)){
            $user->setDelpeId($delpe_id);
        }

        $user->setSsoSyncData($userData);

        return $user;
    }



    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function encryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = $this->params->get('encryption_iv');

        // Store the encryption key
        $encryption_key = $this->params->get('encryption_key');
        
        // Use openssl_encrypt() function to encrypt the data
        $encryption = openssl_encrypt($parameter, $ciphering,
            $encryption_key, $options, $encryption_iv);

        return urlencode($encryption);
    }
    
    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function decryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";
    
        // Use OpenSSl Encryption method
        $options = 0;
    
        $decryption_iv = $this->params->get('encryption_iv');
    
        // Store the decryption key
        $decryption_key = $this->params->get('encryption_key');
    
        // Use openssl_decrypt() function to decrypt the data
        return openssl_decrypt ($parameter, $ciphering,
            $decryption_key, $options, $decryption_iv);
    }
}
