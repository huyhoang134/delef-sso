<?php

namespace App\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtypeRepository")
 * @UniqueEntity(fields="label", message="Label is already taken.")
 */
class Wtype
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $label;

 

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", options={"default":0},nullable=true)
     */
    private $status;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activation_civile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activation_scolaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introduction;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introduction_ouvert;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introduction_ferme;

    /**
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $actif;
    

     /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wconf", mappedBy="wconf_typcont")
     * @ORM\JoinColumn( name="wtype_wconf", referencedColumnName="wconf_typcont", nullable=true, onDelete="SET NULL"))
     */
    private $wtype_wconf;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comptes_demo;
    
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introduction_ouvert_complement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message_rappel_coordonnees;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message_rappel_declaration;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message_rappel_introduction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $departement;

    /**
     * @ORM\Column(type="integer")
     */
    private $priorite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_email;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_email2;
    
    /**
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $send_validation_email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $declaration_aide;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email_signature;

    /**
     * @ORM\Column(type="integer")
     */
    private $archive;

    /**
     * @ORM\Column(type="integer")
     */
    private $archive_ouverture;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $historique_intro_civile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $historique_intro_scolaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $proformat_intro;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $proformat_confirmation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $validation_confirm_message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label_interface;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label_interface_home;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type_interface;

    public function __construct()
    {
        //$this->type = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getLabel();
    }
    
    public function unsetId()
    {
        $this->id=null;
    } 
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Wdeclar[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addTypes(Wdeclar $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
            $types->setWdeTypcont($this);
        }

        return $this;
    }

    public function removeTypes(Wdeclar $types): self
    {
        if ($this->types->contains($types)) {
            $this->types->removeElement($types);
            // set the owning side to null (unless already changed)
            if ($types->getWdeTypcont() === $this) {
                $types->setWdeTypcont(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }


    public function getActivationCivile(): ?string
    {
        return $this->activation_civile;
    }

    public function setActivationCivile(?string $activation_civile): self
    {
        $this->activation_civile = $activation_civile;

        return $this;
    }

    public function getActivationScolaire(): ?string
    {
        return $this->activation_scolaire;
    }

    public function setActivationScolaire(?string $activation_scolaire): self
    {
        $this->activation_scolaire = $activation_scolaire;

        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(?string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }



    public function getIntroductionOuvert(): ?string
    {
        return $this->introduction_ouvert;
    }

    public function setIntroductionOuvert(?string $introduction_ouvert): self
    {
        $this->introduction_ouvert = $introduction_ouvert;

        return $this;
    }

    public function getIntroductionFerme(): ?string
    {
        return $this->introduction_ferme;
    }

    public function setIntroductionFerme(?string $introduction_ferme): self
    {
        $this->introduction_ferme = $introduction_ferme;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }
    
    
   /**
     * @return Collection|Wconf[]
     */
    public function getWtypeWconf(): Collection
    {
        return $this->wtype_wconf;
    }

    public function addWtypeWconf(Wdeclar $WtypeWconf): self
    {
        if (!$this->wtype_wconf->contains($WtypeWconf)) {
            $this->wtype_wconf[] = $WtypeWconf;
            $types->setWconfTypcont($this);
        }

        return $this;
    }

    public function removeWtypeWconf(Wdeclar $WtypeWconf): self
    {
        if ($this->wtype_wconf->contains($WtypeWconf)) {
            $this->wtype_wconf->removeElement($WtypeWconf);
            // set the owning side to null (unless already changed)
            if ($WtypeWconf->getWconfTypcont() === $this) {
                $WtypeWconf->setWconfTypcont(null);
            }
        }

        return $this;
    }

    public function getComptesDemo(): ?string
    {
        return $this->comptes_demo;
    }

    public function setComptesDemo(?string $comptes_demo): self
    {
        $this->comptes_demo = $comptes_demo;

        return $this;
    }
    
    public function getIntroductionOuvertComplement(): ?string
    {
        return $this->introduction_ouvert_complement;
    }
    
    public function setIntroductionOuvertComplement(?string $introduction_ouvert_complement): self
    {
        $this->introduction_ouvert_complement = $introduction_ouvert_complement;
    
        return $this;
    }

    public function getMessageRappelCoordonnees(): ?string
    {
        return $this->message_rappel_coordonnees;
    }

    public function setMessageRappelCoordonnees(?string $message_rappel_coordonnees): self
    {
        $this->message_rappel_coordonnees = $message_rappel_coordonnees;

        return $this;
    }

    public function getMessageRappelDeclaration(): ?string
    {
        return $this->message_rappel_declaration;
    }

    public function setMessageRappelDeclaration(?string $message_rappel_declaration): self
    {
        $this->message_rappel_declaration = $message_rappel_declaration;

        return $this;
    }

    public function getMessageRappelIntroduction(): ?string
    {
        return $this->message_rappel_introduction;
    }

    public function setMessageRappelIntroduction(?string $message_rappel_introduction): self
    {
        $this->message_rappel_introduction = $message_rappel_introduction;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getPriorite(): ?int
    {
        return $this->priorite;
    }

    public function setPriorite(?int $priorite): self
    {
        $this->priorite = $priorite;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }

    public function setContactEmail(?string $contact_email): self
    {
        $this->contact_email = $contact_email;

        return $this;
    }

    public function getContactEmail2(): ?string
    {
        return $this->contact_email2;
    }
    
    public function setContactEmail2(?string $contact_email2): self
    {
        $this->contact_email2 = $contact_email2;
    
        return $this;
    }
    
    
    public function getSendValidationEmail(): ?bool
    {
        return $this->send_validation_email;
    }
    
    public function setSendValidationEmail(bool $send_validation_email): self
    {
        $this->send_validation_email = $send_validation_email;
    
        return $this;
    }
    
    
    public function getDeclarationAide(): ?array
    {
        return !empty($this->declaration_aide)?unserialize($this->declaration_aide):[];
    }

    public function setDeclarationAide(?array $declaration_aide): self
    {
        $this->declaration_aide = serialize($declaration_aide);
        return $this;
    }

    public function getEmailSignature(): ?string
    {
        return $this->email_signature;
    }

    public function setEmailSignature(string $email_signature): self
    {
        $this->email_signature = $email_signature;

        return $this;
    }

    public function getArchive(): ?int
    {
        return $this->archive;
    }

    public function setArchive(int $archive): self
    {
        $this->archive = $archive;

        return $this;
    }

    public function getArchiveOuverture(): ?int
    {
        return $this->archive_ouverture;
    }

    public function setArchiveOuverture(int $archive_ouverture): self
    {
        $this->archive_ouverture = $archive_ouverture;

        return $this;
    }

    public function getHistoriqueIntroCivile(): ?string
    {
        return $this->historique_intro_civile;
    }

    public function setHistoriqueIntroCivile(?string $historique_intro_civile): self
    {
        $this->historique_intro_civile = $historique_intro_civile;

        return $this;
    }

    public function getHistoriqueIntroScolaire(): ?string
    {
        return $this->historique_intro_scolaire;
    }

    public function setHistoriqueIntroScolaire(?string $historique_intro_scolaire): self
    {
        $this->historique_intro_scolaire = $historique_intro_scolaire;

        return $this;
    }

    public function getProformatIntro(): ?string
    {
        return $this->proformat_intro;
    }

    public function setProformatIntro(?string $proformat_intro): self
    {
        $this->proformat_intro = $proformat_intro;

        return $this;
    }

    public function getProformatConfirmation(): ?string
    {
        return $this->proformat_confirmation;
    }

    public function setProformatConfirmation(?string $proformat_confirmation): self
    {
        $this->proformat_confirmation = $proformat_confirmation;

        return $this;
    }

    public function getValidationConfirmMessage(): ?string
    {
        return $this->validation_confirm_message;
    }

    public function setValidationConfirmMessage(?string $validation_confirm_message): self
    {
        $this->validation_confirm_message = $validation_confirm_message;

        return $this;
    }

    public function getLabelInterface(): ?string
    {
        return $this->label_interface;
    }

    public function setLabelInterface(?string $label_interface): self
    {
        $this->label_interface = $label_interface;

        return $this;
    }

    public function getLabelInterfaceHome(): ?string
    {
        return $this->label_interface_home;
    }
    
    public function setLabelInterfaceHome(?string $label_interface_home): self
    {
        $this->label_interface_home = $label_interface_home;
    
        return $this;
    }
    
    public function getTypeInterface(): ?int
    {
        return $this->type_interface;
    }

    public function setTypeInterface(?int $type_interface): self
    {
        $this->type_interface = $type_interface;

        return $this;
    }
}
