<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WconfRepository")
 */
class Wconf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wtype", inversedBy="id")
     * @ORM\JoinColumn( name="wconf_typcont", referencedColumnName="id", nullable=true, onDelete="SET NULL"))
     */
    private $wconf_typcont;

    /**
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $actif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_objet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $label_visible;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etape;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $synchro_field;


    /**
     * ###@Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $type_objet_conf;

    /**
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $mandatory;

    /**
     * @ORM\Column(type="integer",options={"default":0})
     */
    private $validation_email;

    /**
     * @ORM\Column(type="boolean",options={"default":1})
     */
    private $affichage_espace_ferme;

    /**
     * @ORM\Column(type="boolean",options={"default":1})
     */
    private $facturation;
    
    public function __construct()
    {
        //$this->type = new ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->getLabel();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function unsetId()
    {
        $this->id=null;
    }
     public function getWconfTypcont(): ?Wtype
    {
        return $this->wconf_typcont;
    }

    public function setWconfTypcont(?Wtype $wconf_typcont): self
    {
        $this->wconf_typcont = $wconf_typcont;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getTypeObjet(): ?string
    {
        return $this->type_objet;
    }

    public function setTypeObjet(?string $type_objet): self
    {
        $this->type_objet = $type_objet;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }


    public function getLabelVisible(): ?bool
    {
        return $this->label_visible;
    }

    public function setLabelVisible(bool $label_visible): self
    {
        $this->label_visible = $label_visible;

        return $this;
    }

    public function getEtape(): ?string
    {
        return $this->etape;
    }

    public function setEtape(?string $etape): self
    {
        $this->etape = $etape;

        return $this;
    }


    public function getSynchroField(): ?string
    {
        return $this->synchro_field;
    }

    public function setSynchroField(?string $synchro_field): self
    {
        $this->synchro_field = $synchro_field;

        return $this;
    }


    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getTypeObjetConf(): ?array
    {
        return !empty($this->type_objet_conf)?unserialize($this->type_objet_conf):[];
    }

    public function setTypeObjetConf(?array $type_objet_conf): self
    {
        $this->type_objet_conf = serialize($type_objet_conf);

        return $this;
    }

    public function getMandatory(): ?bool
    {
        return $this->mandatory;
    }

    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }


    public function getValidationEmail(): ?int
    {
        return $this->validation_email;
    }

    public function setValidationEmail(int $validation_email): self
    {
        $this->validation_email = $validation_email;

        return $this;
    }

    public function getAffichageEspaceFerme(): ?bool
    {
        return $this->affichage_espace_ferme;
    }
    
    public function setAffichageEspaceFerme(bool $affichage_espace_ferme): self
    {
        $this->affichage_espace_ferme = $affichage_espace_ferme;
    
        return $this;
    }

    public function getFacturation(): ?bool
    {
        return $this->facturation;
    }

    public function setFacturation(bool $facturation): self
    {
        $this->facturation = $facturation;

        return $this;
    }
}
