<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wcontact
 *
 * @ORM\Table(name="wcontact", indexes={@ORM\Index(name="NomPrenom", columns={"NomPrenom"}), @ORM\Index(name="Actif", columns={"Actif"})})
 * @ORM\Entity
 */
class Wcontact
{
    /**
     * @var string
     *
     * @ORM\Column(name="PkContact", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkcontact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DateSQL", type="string", length=255, nullable=true)
     */
    private $datesql;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NomPrenom", type="string", length=255, nullable=true)
     */
    private $nomprenom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TitreMailing", type="string", length=255, nullable=true)
     */
    private $titremailing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Fonction", type="string", length=255, nullable=true)
     */
    private $fonction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Service", type="string", length=255, nullable=true)
     */
    private $service;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Tel1", type="string", length=255, nullable=true)
     */
    private $tel1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Tel2", type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FkSiteGeo", type="string", length=255, nullable=true)
     */
    private $fksitegeo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FkCocontractant", type="string", length=255, nullable=true)
     */
    private $fkcocontractant;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Actif", type="string", length=255, nullable=true)
     */
    private $actif;

    public function getPkcontact(): ?string
    {
        return $this->pkcontact;
    }

    public function getDatesql(): ?string
    {
        return $this->datesql;
    }

    public function setDatesql(?string $datesql): self
    {
        $this->datesql = $datesql;

        return $this;
    }

    public function getNomprenom(): ?string
    {
        return $this->nomprenom;
    }

    public function setNomprenom(?string $nomprenom): self
    {
        $this->nomprenom = $nomprenom;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTitremailing(): ?string
    {
        return $this->titremailing;
    }

    public function setTitremailing(?string $titremailing): self
    {
        $this->titremailing = $titremailing;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(?string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(?string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFksitegeo(): ?string
    {
        return $this->fksitegeo;
    }

    public function setFksitegeo(?string $fksitegeo): self
    {
        $this->fksitegeo = $fksitegeo;

        return $this;
    }

    public function getFkcocontractant(): ?string
    {
        return $this->fkcocontractant;
    }

    public function setFkcocontractant(?string $fkcocontractant): self
    {
        $this->fkcocontractant = $fkcocontractant;

        return $this;
    }

    public function getActif(): ?string
    {
        return $this->actif;
    }

    public function setActif(?string $actif): self
    {
        $this->actif = $actif;

        return $this;
    }


}
