<?php

namespace App\Entity;

use App\Repository\WdelefRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WdelefRepository::class)
 */
class Wdelef
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column("wdf_declar", type="integer", nullable=false)
     */
    private $wdf_declar;

    /**
     * @ORM\Column(type="datetime")
     */
    private $wdf_stamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wdf_synchro;

    /**
     * @ORM\Column(type="integer")
     */
    private $wdf_dossier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wcontrat", inversedBy="wdelefs")
     * @ORM\JoinColumn( name="wdf_contrat", referencedColumnName="wct_contrat", nullable=true, onDelete="SET NULL"))
     */
    private $wdfContrat;

    /**
     * @ORM\Column(type="integer")
     */
    private $wdf_annee;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $wdf_annee_decl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_eleves;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $wdf_tranche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_copieurs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_profs;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $wdf_etat_declar;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wdf_date_validation;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wdf_email_sent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_effectif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche01;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche02;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche03;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche04;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche05;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche06;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche07;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wdf_intitule_decl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche08;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche09;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche10;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche11;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche12;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche13;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche14;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche15;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche16
    ;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_effectif2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_effectif3;



    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_effectif4;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche17;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche18;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche19;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche20;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche21;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdf_tranche22;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wdf_numperiod;




    public function getWdfStamp(): ?\DateTimeInterface
    {
        return $this->wdf_stamp;
    }

    public function setWdfStamp(\DateTimeInterface $wdf_stamp): self
    {
        $this->wdf_stamp = $wdf_stamp;

        return $this;
    }

    public function getWdfSynchro(): ?\DateTimeInterface
    {
        return $this->wdf_synchro;
    }

    public function setWdfSynchro(?\DateTimeInterface $wdf_synchro): self
    {
        $this->wdf_synchro = $wdf_synchro;

        return $this;
    }

    public function getWdfDeclar(): ?int
    {
        return $this->wdf_declar;
    }

    public function getWdfDossier(): ?int
    {
        return $this->wdf_dossier;
    }

    public function setWdfDossier(int $wdf_dossier): self
    {
        $this->wdf_dossier = $wdf_dossier;

        return $this;
    }

    public function getWdfContrat(): ?int
    {
        return $this->wdfContrat;
    }

    public function setWdfContrat(int $wdfContrat): self
    {
        $this->wdfContrat = $wdfContrat;

        return $this;
    }

    public function getWdfAnnee(): ?int
    {
        return $this->wdf_annee;
    }

    public function setWdfAnnee(int $wdf_annee): self
    {
        $this->wdf_annee = $wdf_annee;

        return $this;
    }

    public function getWdfAnneedecl(): ?string
    {
        return $this->wdf_annee_decl;
    }

    public function setWdfAnneedecl(string $wdf_annee_decl): self
    {
        $this->wdf_annee_decl = $wdf_annee_decl;

        return $this;
    }

    public function getWdfEleves(): ?int
    {
        return $this->wdf_eleves;
    }

    public function setWdfEleves(?int $wdf_eleves): self
    {
        $this->wdf_eleves = $wdf_eleves;

        return $this;
    }

    public function getWdfTranche(): ?string
    {
        return $this->wdf_tranche;
    }

    public function setWdfTranche(?string $wdf_tranche): self
    {
        $this->wdf_tranche = $wdf_tranche;

        return $this;
    }

    public function getWdfCopieurs(): ?int
    {
        return $this->wdf_copieurs;
    }

    public function setWdfCopieurs(?int $wdf_copieurs): self
    {
        $this->wdf_copieurs = $wdf_copieurs;

        return $this;
    }

    public function getWdfProfs(): ?int
    {
        return $this->wdf_profs;
    }

    public function setWdfProfs(?int $wdf_profs): self
    {
        $this->wdf_profs = $wdf_profs;

        return $this;
    }

    public function getWdfEtatdeclar(): ?string
    {
        return $this->wdf_etat_declar;
    }

    public function setWdfEtatdeclar(string $wdf_etat_declar): self
    {
        $this->wdf_etat_declar = $wdf_etat_declar;

        return $this;
    }

    public function getWdfDatevalidation(): ?\DateTimeInterface
    {
        return $this->wdf_date_validation;
    }

    public function setWdfDatevalidation(?\DateTimeInterface $wdf_date_validation): self
    {
        $this->wdf_date_validation = $wdf_date_validation;

        return $this;
    }

    public function getWdfEmailsent(): ?int
    {
        return $this->wdf_email_sent;
    }

    public function setWdfEmailsent(?int $wdf_email_sent): self
    {
        $this->wdf_email_sent = $wdf_email_sent;

        return $this;
    }

    public function getWdfEffectif(): ?int
    {
        return $this->wdf_effectif;
    }

    public function setWdfEffectif(?int $wdf_effectif): self
    {
        $this->wdf_effectif = $wdf_effectif;

        return $this;
    }

    public function getWdfTranche01(): ?int
    {
        return $this->wdf_tranche01;
    }

    public function setWdfTranche01(?int $wdf_tranche01): self
    {
        $this->wdf_tranche01 = $wdf_tranche01;

        return $this;
    }

    public function getWdfTranche02(): ?int
    {
        return $this->wdf_tranche02;
    }

    public function setWdfTranche02(?int $wdf_tranche02): self
    {
        $this->wdf_tranche02 = $wdf_tranche02;

        return $this;
    }

    public function getWdfTranche03(): ?int
    {
        return $this->wdf_tranche03;
    }

    public function setWdfTranche03(?int $wdf_tranche03): self
    {
        $this->wdf_tranche03 = $wdf_tranche03;

        return $this;
    }

    public function getWdfTranche04(): ?int
    {
        return $this->wdf_tranche04;
    }

    public function setWdfTranche04(?int $wdf_tranche04): self
    {
        $this->wdf_tranche04 = $wdf_tranche04;

        return $this;
    }

    public function getWdfTranche05(): ?int
    {
        return $this->wdf_tranche05;
    }

    public function setWdfTranche05(?int $wdf_tranche05): self
    {
        $this->wdf_tranche05 = $wdf_tranche05;

        return $this;
    }

    public function getWdfTranche06(): ?int
    {
        return $this->wdf_tranche06;
    }

    public function setWdfTranche06(?int $wdf_tranche06): self
    {
        $this->wdf_tranche06 = $wdf_tranche06;

        return $this;
    }

    public function getWdfTranche07(): ?int
    {
        return $this->wdf_tranche07;
    }

    public function setWdfTranche07(?int $wdf_tranche07): self
    {
        $this->wdf_tranche07 = $wdf_tranche07;

        return $this;
    }

    public function getWdfIntituledecl(): ?string
    {
        return $this->wdf_intitule_decl;
    }

    public function setWdfIntituledecl(?string $wdf_intitule_decl): self
    {
        $this->wdf_intitule_decl = $wdf_intitule_decl;

        return $this;
    }

    public function getWdfTranche08(): ?int
    {
        return $this->wdf_tranche08;
    }

    public function setWdfTranche08(?int $wdf_tranche08): self
    {
        $this->wdf_tranche08 = $wdf_tranche08;

        return $this;
    }

    public function getWdfTranche09(): ?int
    {
        return $this->wdf_tranche09;
    }

    public function setWdfTranche09(?int $wdf_tranche09): self
    {
        $this->wdf_tranche09 = $wdf_tranche09;

        return $this;
    }

    public function getWdfTranche10(): ?int
    {
        return $this->wdf_tranche10;
    }

    public function setWdfTranche10(?int $wdf_tranche10): self
    {
        $this->wdf_tranche10 = $wdf_tranche10;

        return $this;
    }

    public function getWdfTranche11(): ?int
    {
        return $this->wdf_tranche11;
    }

    public function setWdfTranche11(?int $wdf_tranche11): self
    {
        $this->wdf_tranche11 = $wdf_tranche11;

        return $this;
    }

    public function getWdfTranche12(): ?int
    {
        return $this->wdf_tranche12;
    }

    public function setWdfTranche12(?int $wdf_tranche12): self
    {
        $this->wdf_tranche12 = $wdf_tranche12;

        return $this;
    }

    public function getWdfTranche13(): ?int
    {
        return $this->wdf_tranche13;
    }

    public function setWdfTranche13(?int $wdf_tranche13): self
    {
        $this->wdf_tranche13 = $wdf_tranche13;

        return $this;
    }

    public function getWdfTranche14(): ?int
    {
        return $this->wdf_tranche14;
    }

    public function setWdfTranche14(?int $wdf_tranche14): self
    {
        $this->wdf_tranche14 = $wdf_tranche14;

        return $this;
    }

    public function getWdfTranche15(): ?int
    {
        return $this->wdf_tranche15;
    }

    public function setWdfTranche15(?int $wdf_tranche15): self
    {
        $this->wdf_tranche15 = $wdf_tranche15;

        return $this;
    }

    public function getWdfTranche16(): ?int
    {
        return $this->wdfTranche16;
    }

    public function setWdfTranche16(?int $wdfTranche16): self
    {
        $this->wdfTranche16 = $wdfTranche16;

        return $this;
    }

    public function getWdfEffectif2(): ?int
    {
        return $this->wdf_effectif2;
    }

    public function setWdfEffectif2(?int $wdf_effectif2): self
    {
        $this->wdf_effectif2 = $wdf_effectif2;

        return $this;
    }

    public function getWdfEffectif3(): ?int
    {
        return $this->wdf_effectif3;
    }

    public function setWdfEffectif3(?int $wdf_effectif3): self
    {
        $this->wdf_effectif3 = $wdf_effectif3;

        return $this;
    }



    public function getWdfEffectif4(): ?int
    {
        return $this->wdf_effectif4;
    }

    public function setWdfEffectif4(?int $wdf_effectif4): self
    {
        $this->wdf_effectif4 = $wdf_effectif4;

        return $this;
    }

    public function getWdfTranche17(): ?int
    {
        return $this->wdf_tranche17;
    }

    public function setWdfTranche17(?int $wdf_tranche17): self
    {
        $this->wdf_tranche17 = $wdf_tranche17;

        return $this;
    }

    public function getWdfTranche18(): ?int
    {
        return $this->wdf_tranche18;
    }

    public function setWdfTranche18(?int $wdf_tranche18): self
    {
        $this->wdf_tranche18 = $wdf_tranche18;

        return $this;
    }

    public function getWdfTranche19(): ?int
    {
        return $this->wdf_tranche19;
    }

    public function setWdfTranche19(?int $wdf_tranche19): self
    {
        $this->wdf_tranche19 = $wdf_tranche19;

        return $this;
    }

    public function getWdfTranche20(): ?int
    {
        return $this->wdf_tranche20;
    }

    public function setWdfTranche20(?int $wdf_tranche20): self
    {
        $this->wdf_tranche20 = $wdf_tranche20;

        return $this;
    }

    public function getWdfTranche21(): ?int
    {
        return $this->wdf_tranche21;
    }

    public function setWdfTranche21(?int $wdf_tranche21): self
    {
        $this->wdf_tranche21 = $wdf_tranche21;

        return $this;
    }

    public function getWdfTranche22(): ?int
    {
        return $this->wdf_tranche22;
    }

    public function setWdfTranche22(?int $wdf_tranche22): self
    {
        $this->wdf_tranche22 = $wdf_tranche22;

        return $this;
    }



    public function getWdfNumperiod(): ?string
    {
        return $this->wdf_numperiod;
    }

    public function setWdfNumperiod(?string $wdf_numperiod): self
    {
        $this->wdf_numperiod = $wdf_numperiod;

        return $this;
    }
}
