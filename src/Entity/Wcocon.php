<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WcoconRepository")
 * @Table(name="wcocon")
 */
class Wcocon implements UserInterface
{


    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     * @ORM\Version
     */
    private $wco_stamp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wco_website;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wco_synchro;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $wco_dossier;
    
    
    /**
     * @ORM\Column(type="string", length=8)
     */
    private $wco_password;

    /**
     * @ORM\Column(type="string", length=55, nullable=true)
     */
    private $wco_raisoc1;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_raisoc2;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_raisoc3;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $wco_numvoie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_adresse1;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_adresse2;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_adresse3;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $wco_copos;

    /**
     * @ORM\Column(type="string", length=42, nullable=true)
     */
    private $wco_ville;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $wco_siret;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wco_tel;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wco_fax;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_president;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_nom_legal;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wco_tit_legal;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wco_fct_legal;

    /**
     * @ORM\Column(type="string", length=90, nullable=true)
     */
    private $wco_entete;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $wco_typetabl;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_mel_legal;

    

     /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_tel_legal;
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_id_ct_legal;
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_id_ct_president;
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wco_id_adr_siege;




    private $sso_sync_data;
    private $delpe_id;

    
    
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wcontrat", mappedBy="wctDossier", cascade={"all"}, orphanRemoval=false)
     */
    private $wcontrats;


    public function __construct()
    {
        //$this->wco_dossier = new ArrayCollection();
        $this->wcontrats = new ArrayCollection();
    }

    public function __toString()
    {
        return "Dossier ".$this->getWcoDossier();
    }
    

    public function getWcoStamp(): ?\DateTimeInterface
    {
        return $this->wco_stamp;
    }

    public function setWcoStamp(\DateTimeInterface $wco_stamp): self
    {
        $this->wco_stamp = $wco_stamp;

        return $this;
    }

    public function getWcoWebsite(): ?string
    {
        return $this->wco_website;
    }

    public function setWcoWebsite(?string $wco_website): self
    {
        $this->wco_website = $wco_website;

        return $this;
    }

    public function getWcoSynchro(): ?\DateTimeInterface
    {
        return $this->wco_synchro;
    }

    public function setWcoSynchro(?\DateTimeInterface $wco_synchro): self
    {
        $this->wco_synchro = $wco_synchro;

        return $this;
    }

    public function getWcoDossier(): ?int
    {
        return $this->wco_dossier;
    }

    public function setWcoDossier(int $wco_dossier): self
    {
        $this->wco_dossier = $wco_dossier;

        return $this;
    }

    public function getWcoPassword(): ?string
    {
        return $this->wco_password;
    }

    public function setWcoPassword(string $wco_password): self
    {
        $this->wco_password = $wco_password;

        return $this;
    }

    public function getWcoRaisoc1(): ?string
    {
        return $this->wco_raisoc1;
    }

    public function setWcoRaisoc1(?string $wco_raisoc1): self
    {
        $this->wco_raisoc1 = $wco_raisoc1;

        return $this;
    }

    public function getWcoRaisoc2(): ?string
    {
        return $this->wco_raisoc2;
    }

    public function setWcoRaisoc2(?string $wco_raisoc2): self
    {
        $this->wco_raisoc2 = $wco_raisoc2;

        return $this;
    }

    public function getWcoRaisoc3(): ?string
    {
        return $this->wco_raisoc3;
    }

    public function setWcoRaisoc3(?string $wco_raisoc3): self
    {
        $this->wco_raisoc3 = $wco_raisoc3;

        return $this;
    }

    public function getWcoNumvoie(): ?string
    {
        return $this->wco_numvoie;
    }

    public function setWcoNumvoie(?string $wco_numvoie): self
    {
        $this->wco_numvoie = $wco_numvoie;

        return $this;
    }

    public function getWcoAdresse1(): ?string
    {
        return $this->wco_adresse1;
    }

    public function setWcoAdresse1(?string $wco_adresse1): self
    {
        $this->wco_adresse1 = $wco_adresse1;

        return $this;
    }

    public function getWcoAdresse2(): ?string
    {
        return $this->wco_adresse2;
    }

    public function setWcoAdresse2(?string $wco_adresse2): self
    {
        $this->wco_adresse2 = $wco_adresse2;

        return $this;
    }

    public function getWcoAdresse3(): ?string
    {
        return $this->wco_adresse3;
    }

    public function setWcoAdresse3(?string $wco_adresse3): self
    {
        $this->wco_adresse3 = $wco_adresse3;

        return $this;
    }

    public function getWcoCopos(): ?string
    {
        return $this->wco_copos;
    }

    public function setWcoCopos(?string $wco_copos): self
    {
        $this->wco_copos = $wco_copos;

        return $this;
    }

    public function getWcoVille(): ?string
    {
        return $this->wco_ville;
    }

    public function setWcoVille(?string $wco_ville): self
    {
        $this->wco_ville = $wco_ville;

        return $this;
    }

    public function getWcoSiret(): ?string
    {
        return $this->wco_siret;
    }

    public function setWcoSiret(?string $wco_siret): self
    {
        $this->wco_siret = $wco_siret;

        return $this;
    }

    public function getWcoTel(): ?string
    {
        return $this->wco_tel;
    }

    public function setWcoTel(?string $wco_tel): self
    {
        $this->wco_tel = $wco_tel;

        return $this;
    }

    public function getWcoFax(): ?string
    {
        return $this->wco_fax;
    }

    public function setWcoFax(?string $wco_fax): self
    {
        $this->wco_fax = $wco_fax;

        return $this;
    }

    public function getWcoEmail(): ?string
    {
        return $this->wco_email;
    }

    public function setWcoEmail(?string $wco_email): self
    {
        $this->wco_email = $wco_email;

        return $this;
    }

    public function getWcoPresident(): ?string
    {
        return $this->wco_president;
    }

    public function setWcoPresident(?string $wco_president): self
    {
        $this->wco_president = $wco_president;

        return $this;
    }

    public function getWcoNomLegal(): ?string
    {
        return $this->wco_nom_legal;
    }

    public function setWcoNomLegal(?string $wco_nom_legal): self
    {
        $this->wco_nom_legal = $wco_nom_legal;

        return $this;
    }

    public function getWcoTitLegal(): ?string
    {
        return $this->wco_tit_legal;
    }

    public function setWcoTitLegal(?string $wco_tit_legal): self
    {
        $this->wco_tit_legal = $wco_tit_legal;

        return $this;
    }

    public function getWcoFctLegal(): ?string
    {
        return $this->wco_fct_legal;
    }

    public function setWcoFctLegal(?string $wco_fct_legal): self
    {
        $this->wco_fct_legal = $wco_fct_legal;

        return $this;
    }

    public function getWcoEntete(): ?string
    {
        return $this->wco_entete;
    }

    public function setWcoEntete(?string $wco_entete): self
    {
        $this->wco_entete = $wco_entete;

        return $this;
    }

    public function getWcoTypetabl(): ?string
    {
        return $this->wco_typetabl;
    }

    public function setWcoTypetabl(?string $wco_typetabl): self
    {
        $this->wco_typetabl = $wco_typetabl;

        return $this;
    }

    public function getWcoTelLegal(): ?string
    {
        return $this->wco_tel_legal;
    }
    
    public function setWcoTelLegal(?string $wco_tel_legal): self
    {
        $this->wco_tel_legal = $wco_tel_legal;
    
        return $this;
    }
    
    public function getWcoMelLegal(): ?string
    {
        return $this->wco_mel_legal;
    }

    public function setWcoMelLegal(?string $wco_mel_legal): self
    {
        $this->wco_mel_legal = $wco_mel_legal;

        return $this;
    }


    public function getWcoIdCtLegal(): ?string
    {
        return $this->wco_id_ct_legal;
    }
    
    public function setWcoIdCtLegal(?string $wco_id_ct_legal): self
    {
        $this->wco_id_ct_legal = $wco_id_ct_legal;
        return $this;
    }

    public function getWcoIdCtPresident(): ?string
    {
        return $this->wco_id_ct_president;
    }
    
    public function setWcoIdCtPresident(?string $wco_id_ct_president): self
    {
        $this->wco_id_ct_president = $wco_id_ct_president;
        return $this;
    }
    
    public function getWcoIdAdrSiege(): ?string
    {
        return $this->wco_id_adr_siege;
    }
    
    public function setWcoIdAdrSiege(?string $wco_id_adr_siege): self
    {
        $this->wco_id_adr_siege = $wco_id_adr_siege;
        return $this;
    }
    

    
    /**
     * @return Collection|Wcontrat[]
     */
    public function getWcontrats(): Collection
    {
        //Reorder declaration to N and V in first.
        $contrats=$this->wcontrats->toArray();

        $contrats_ord=[];
        #first order by ID.
        foreach($contrats as $d){
            $contrats_ord[$d->getWctContrat()]=$d;
        }
        
        rsort($contrats_ord);
        
        #then reorder by status.
        foreach($contrats_ord as $d){
            if($d->getWdelefs()[0]->getWdfEtatDeclar()=='N' || $d->getWdelefs()[0]->getWdfEtatDeclar()=='V'){
                array_unshift($contrats, $d);
            }
        }
        //$new_delacrs = array_unique($contrats);
        
        $this->wcontrats = new ArrayCollection();
        foreach($contrats as $d){
            $this->addWcontrat($d);
        }
        return $this->wcontrats;
    }
    
    public function getWcontratsArray()
    {
        
        return $this->wcontrats->toArray();
    }

    public function addWcontrat(Wcontrat $wcontrat): self
    {
        if (!$this->wcontrats->contains($wcontrat)) {
            $this->wcontrats[] = $wcontrat;
            $wcontrat->setWctDossier($this);
        }

        return $this;
    }

    public function removeWcontrat(Wcontrat $wcontrat): self
    {

        if ($this->wcontrats->contains($wcontrat)) {
            $this->wcontrats->removeElement($wcontrat);
            // set the owning side to null (unless already changed)
            if ($wcontrat->getWctDossier() === $this) {
                $wcontrat->setWctDossier(null);
            }
        }

        return $this;
    }


    public function setWcontrat($wcontrats,$current)
    {
        foreach ($current as $w) {
            if(!$wcontrats->contains($w)){
                $w->setWdeDossier(null);
            }
           
        }
        $this->wcontrats = new ArrayCollection();
        
        foreach ($wcontrats as $wcontrat) {
            $this->addWdeclar($wcontrat);
        }

    }
    
    
    

    
    
    
    
    
    /*Authentication méthods implements interface User*/
    public function getRoles(){
        return ['ROLE_USER'];
    }
    

    public function getPassword(){
        return $this->getWcoPassword();
    }

    public function getSalt() {
        return false;
    }
    

    public function getUsername(){
        return $this->getWcoDossier();
    }
    

    public function eraseCredentials(){
        
    }
    
    
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }
    
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }


    public function  getSsoSyncData(){
        return $this->sso_sync_data;
    }

    public function  setSsoSyncData($data){
        $this->sso_sync_data=$data;
    }

    public function getDelpeId(){
        return $this->delpe_id;
    }

    public function  setDelpeId($delpe_id){
        $this->delpe_id=$delpe_id;
    }





}
