<?php

namespace App\Entity;

use App\Repository\WcontratRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WcontratRepository::class)
 */
class Wcontrat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $wct_contrat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $wctStamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wctSynchro;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $wct_typcont;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_nom_resp;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wct_tit_resp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_fct_resp;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wct_mel_resp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_nom_sec_gen;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wct_tit_sec_gen;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_fct_sec_gen;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wct_mel_sec_gen;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wct_tel_resp;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wct_fax_resp;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wct_is_chorus_pro;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $wct_siret;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wct_service_code;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_order_number;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_nom_fact;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wct_tit_fact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wct_fct_fact;

    /**
     * @ORM\Column(name="wct_raisoc1_fact" , type="string", length=50, nullable=true)
     */
    private $wct_raisoc1_fact;

    /**
     * @ORM\Column(name="wct_adresse1_fact" ,type="string", length=50, nullable=true)
     */
    private $wct_adresse1_fact;

    /**
     * @ORM\Column(name="wct_adresse2_fact" ,type="string", length=50, nullable=true)
     */
    private $wct_adresse2_fact;

    /**
     * @ORM\Column(name="wct_adresse3_fact" ,type="string", length=50, nullable=true)
     */
    private $wct_adresse3_fact;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $wct_copos_fact;

    /**
     * @ORM\Column(type="string", length=42, nullable=true)
     */
    private $wct_ville_fact;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wct_tel_fact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_fact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_nom_sec_gen;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_resp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_adr_fact;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $wct_password;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wct_is_order_number;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wct_mail_fact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_nom_sec_gen_question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_resp_question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_ct_fact_question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wct_id_adr_fact_question;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wcocon", inversedBy="wcontrats")
     * @ORM\JoinColumn( name="wct_dossier", referencedColumnName="wco_dossier", nullable=true, onDelete="SET NULL"))
     */

    private $wctDossier;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wdelef", mappedBy="wdfContrat", cascade={"all"}, orphanRemoval=false)
     */
    private $wdelefs;

    public function __construct()
    {
        $this->wdelefs = new ArrayCollection();
    }

    public function getWctStamp(): ?\DateTimeInterface
    {
        return $this->wctStamp;
    }

    public function setWctStamp(\DateTimeInterface $wctStamp): self
    {
        $this->wctStamp = $wctStamp;

        return $this;
    }

    public function getWctSynchro(): ?\DateTimeInterface
    {
        return $this->wctSynchro;
    }

    public function setWctSynchro(?\DateTimeInterface $wctSynchro): self
    {
        $this->wctSynchro = $wctSynchro;

        return $this;
    }

    public function getWctDossier(): ?Wcocon
    {
        return $this->wctDossier;
    }

    public function setWctDossier(?Wcocon $wctDossier): self
    {
        $this->wctDossier = $wctDossier;

        return $this;
    }

    public function getWctContrat(): ?int
    {
        return $this->wct_contrat;
    }

    public function getWctTypcont(): ?string
    {
        return $this->wct_typcont;
    }

    public function setWctTypcont(string $wct_typcont): self
    {
        $this->wct_typcont = $wct_typcont;

        return $this;
    }

    public function getWctNomresp(): ?string
    {
        return $this->wct_nom_resp;
    }

    public function setWctNomresp(?string $wct_nom_resp): self
    {
        $this->wct_nom_resp = $wct_nom_resp;

        return $this;
    }

    public function getWctTitresp(): ?string
    {
        return $this->wct_tit_resp;
    }

    public function setWctTitresp(?string $wct_tit_resp): self
    {
        $this->wct_tit_resp = $wct_tit_resp;

        return $this;
    }

    public function getWctFctresp(): ?string
    {
        return $this->wct_fct_resp;
    }

    public function setWctFctresp(?string $wct_fct_resp): self
    {
        $this->wct_fct_resp = $wct_fct_resp;

        return $this;
    }

    public function getWctMelresp(): ?string
    {
        return $this->wct_mel_resp;
    }

    public function setWctMelresp(?string $wct_mel_resp): self
    {
        $this->wct_mel_resp = $wct_mel_resp;

        return $this;
    }

    public function getWctNomsecgen(): ?string
    {
        return $this->wct_nom_sec_gen;
    }

    public function setWctNomsecgen(?string $wct_nom_sec_gen): self
    {
        $this->wct_nom_sec_gen = $wct_nom_sec_gen;

        return $this;
    }

    public function getWctTitsecgen(): ?string
    {
        return $this->wct_tit_sec_gen;
    }

    public function setWctTitsecgen(?string $wct_tit_sec_gen): self
    {
        $this->wct_tit_sec_gen = $wct_tit_sec_gen;

        return $this;
    }

    public function getWctFctsecgen(): ?string
    {
        return $this->wct_fct_sec_gen;
    }

    public function setWctFctsecgen(?string $wct_fct_sec_gen): self
    {
        $this->wct_fct_sec_gen = $wct_fct_sec_gen;

        return $this;
    }

    public function getWctMelsecgen(): ?string
    {
        return $this->wct_mel_sec_gen;
    }

    public function setWctMelsecgen(?string $wct_mel_sec_gen): self
    {
        $this->wct_mel_sec_gen = $wct_mel_sec_gen;

        return $this;
    }

    public function getWctTelresp(): ?string
    {
        return $this->wct_tel_resp;
    }

    public function setWctTelresp(?string $wct_tel_resp): self
    {
        $this->wct_tel_resp = $wct_tel_resp;

        return $this;
    }

    public function getWctFaxresp(): ?string
    {
        return $this->wct_fax_resp;
    }

    public function setWctFaxresp(?string $wct_fax_resp): self
    {
        $this->wct_fax_resp = $wct_fax_resp;

        return $this;
    }

    public function getWctIschoruspro(): ?int
    {
        return $this->wct_is_chorus_pro;
    }

    public function setWctIschoruspro(int $wct_is_chorus_pro): self
    {
        $this->wct_is_chorus_pro = $wct_is_chorus_pro;

        return $this;
    }

    public function getWctSiret(): ?string
    {
        return $this->wct_siret;
    }

    public function setWctSiret(?string $wct_siret): self
    {
        $this->wct_siret = $wct_siret;

        return $this;
    }

    public function getWctServicecode(): ?string
    {
        return $this->wct_service_code;
    }

    public function setWctServicecode(string $wct_service_code): self
    {
        $this->wct_service_code = $wct_service_code;

        return $this;
    }

    public function getWctOrdernumber(): ?string
    {
        return $this->wct_order_number;
    }

    public function setWctOrdernumber(?string $wct_order_number): self
    {
        $this->wct_order_number = $wct_order_number;

        return $this;
    }

    public function getWctNomfact(): ?string
    {
        return $this->wct_nom_fact;
    }

    public function setWctNomfact(?string $wct_nom_fact): self
    {
        $this->wct_nom_fact = $wct_nom_fact;

        return $this;
    }

    public function getWctTitfact(): ?string
    {
        return $this->wct_tit_fact;
    }

    public function setWctTitfact(?string $wct_tit_fact): self
    {
        $this->wct_tit_fact = $wct_tit_fact;

        return $this;
    }

    public function getWctFctfact(): ?string
    {
        return $this->wct_fct_fact;
    }

    public function setWctFctfact(?string $wct_fct_fact): self
    {
        $this->wct_fct_fact = $wct_fct_fact;

        return $this;
    }

    public function getWctRaisoc1Fact(): ?string
    {
        return $this->wct_raisoc1_fact;
    }

    public function setWctRaisoc1Fact(?string $wct_raisoc1_fact): self
    {
        $this->wct_raisoc1_fact = $wct_raisoc1_fact;

        return $this;
    }

    public function getWctAdresse1Fact(): ?string
    {
        return $this->wct_adresse1_fact;
    }

    public function setWctAdresse1Fact(?string $wct_adresse1_fact): self
    {
        $this->wct_adresse1_fact = $wct_adresse1_fact;

        return $this;
    }

    public function getWctAdresse2Fact(): ?string
    {
        return $this->wct_adresse2_fact;
    }

    public function setWctAdresse2Fact(?string $wct_adresse2_fact): self
    {
        $this->wct_adresse2_fact = $wct_adresse2_fact;

        return $this;
    }

    public function getWctAdresse3Fact(): ?string
    {
        return $this->wct_adresse3_fact;
    }

    public function setWctAdresse3Fact(?string $wct_adresse3_fact): self
    {
        $this->wct_adresse3_fact = $wct_adresse3_fact;

        return $this;
    }

    public function getWctCoposfact(): ?string
    {
        return $this->wct_copos_fact;
    }

    public function setWctCoposfact(?string $wct_copos_fact): self
    {
        $this->wct_copos_fact = $wct_copos_fact;

        return $this;
    }

    public function getWctVillefact(): ?string
    {
        return $this->wct_ville_fact;
    }

    public function setWctVillefact(?string $wct_ville_fact): self
    {
        $this->wct_ville_fact = $wct_ville_fact;

        return $this;
    }

    public function getWctTelfact(): ?string
    {
        return $this->wct_tel_fact;
    }

    public function setWctTelfact(?string $wct_tel_fact): self
    {
        $this->wct_tel_fact = $wct_tel_fact;

        return $this;
    }

    public function getWctIdctfact(): ?int
    {
        return $this->wct_id_ct_fact;
    }

    public function setWctIdctfact(?int $wct_id_ct_fact): self
    {
        $this->wct_id_ct_fact = $wct_id_ct_fact;

        return $this;
    }

    public function getWctIdctnomsecgen(): ?int
    {
        return $this->wct_id_ct_nom_sec_gen;
    }

    public function setWctIdctnomsecgen(?int $wct_id_ct_nom_sec_gen): self
    {
        $this->wct_id_ct_nom_sec_gen = $wct_id_ct_nom_sec_gen;

        return $this;
    }

    public function getWctIdctresp(): ?int
    {
        return $this->wct_id_ct_resp;
    }

    public function setWctIdctresp(?int $wct_id_ct_resp): self
    {
        $this->wct_id_ct_resp = $wct_id_ct_resp;

        return $this;
    }

    public function getWctIdadrfact(): ?int
    {
        return $this->wct_id_adr_fact;
    }

    public function setWctIdadrfact(?int $wct_id_adr_fact): self
    {
        $this->wct_id_adr_fact = $wct_id_adr_fact;

        return $this;
    }

    public function getWctPassword(): ?string
    {
        return $this->wct_password;
    }

    public function setWctPassword(?string $wct_password): self
    {
        $this->wct_password = $wct_password;

        return $this;
    }

    public function getWctIsordernumber(): ?int
    {
        return $this->wct_is_order_number;
    }

    public function setWctIsordernumber(?int $wct_is_order_number): self
    {
        $this->wct_is_order_number = $wct_is_order_number;

        return $this;
    }

    public function getWctMailfact(): ?string
    {
        return $this->wct_mail_fact;
    }

    public function setWctMailfact(?string $wct_mail_fact): self
    {
        $this->wct_mail_fact = $wct_mail_fact;

        return $this;
    }

    public function getWctIdctnomsecgenquestion(): ?int
    {
        return $this->wct_id_ct_nom_sec_gen_question;
    }

    public function setWctIdctnomsecgenquestion(?int $wct_id_ct_nom_sec_gen_question): self
    {
        $this->wct_id_ct_nom_sec_gen_question = $wct_id_ct_nom_sec_gen_question;

        return $this;
    }

    public function getWctIdctrespquestion(): ?int
    {
        return $this->wct_id_ct_resp_question;
    }

    public function setWctIdctrespquestion(?int $wct_id_ct_resp_question): self
    {
        $this->wct_id_ct_resp_question = $wct_id_ct_resp_question;

        return $this;
    }

    public function getWctIdctfactquestion(): ?int
    {
        return $this->wct_id_ct_fact_question;
    }

    public function setWctIdctfactquestion(?int $wct_id_ct_fact_question): self
    {
        $this->wct_id_ct_fact_question = $wct_id_ct_fact_question;

        return $this;
    }

    public function getWctIdadrfactquestion(): ?int
    {
        return $this->wct_id_adr_fact_question;
    }

    public function setWctIdadrfactquestion(?int $wct_id_adr_fact_question): self
    {
        $this->wct_id_adr_fact_question = $wct_id_adr_fact_question;

        return $this;
    }




    /**
     * @return Collection|Wdelef[]
     */
    public function getWdelefs(): Collection
    {
        //Reorder declaration to N and V in first.
        $wdelefs=$this->wdelefs->toArray();

        return $this->wdelefs;
    }

    public function getWdelefsArray()
    {

        return $this->wdelefs->toArray();
    }

    public function addWdelef(Wdelef $wdelef): self
    {
        if (!$this->wdelefs->contains($wdelef)) {
            $this->wdelefs[] = $wdelef;
            $wdelef->setWctDossier($this);
        }

        return $this;
    }


    public function removeWdelef(Wdelef $wdelef): self
    {

        if ($this->wdelefs->contains($wdelef)) {
            $this->wdelefs->removeElement($wdelef);
            // set the owning side to null (unless already changed)
            if ($wdelef->getWdfDossier() === $this) {
                $wdelef->settWdfDossier(null);
            }
        }

        return $this;
    }


    public function setWdelef($wdelefs,$current)
    {
        foreach ($current as $w) {
            if(!$wdelefs->contains($w)){
                $w->setWdfDossier(null);
            }

        }
        $this->wdelefs = new ArrayCollection();

        foreach ($wdelefs as $wdelef) {
            $this->addWdfDossier($wdelef);
        }

    }
}
