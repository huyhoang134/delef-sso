<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChorusRepository")
 */
class Chorus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type_identifiant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $identifiant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raison_sociale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pays_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pays_libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $service_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $service_nom;

    /**
     * @ORM\Column(type="boolean")
     */
    private $service_gestion_EGMT;

    /**
     * @ORM\Column(type="boolean")
     */
    private $service_service_actif;

    /**
     * @ORM\Column(type="boolean")
     */
    private $emetteur_edi;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recepteur_edi;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gestion_statut_mise_en_paiement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gestion_engagement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gestion_service;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gestion_service_engagement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $est_MOA;

    /**
     * @ORM\Column(type="boolean")
     */
    private $est_MOA_uniquement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $structure_active;

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeIdentifiant(): ?int
    {
        return $this->type_identifiant;
    }

    public function setTypeIdentifiant(?int $type_identifiant): self
    {
        $this->type_identifiant = $type_identifiant;

        return $this;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(?string $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raison_sociale;
    }

    public function setRaisonSociale(?string $raison_sociale): self
    {
        $this->raison_sociale = $raison_sociale;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPaysCode(): ?string
    {
        return $this->pays_code;
    }

    public function setPaysCode(?string $pays_code): self
    {
        $this->pays_code = $pays_code;

        return $this;
    }

    public function getPaysLibelle(): ?string
    {
        return $this->pays_libelle;
    }

    public function setPaysLibelle(?string $pays_libelle): self
    {
        $this->pays_libelle = $pays_libelle;

        return $this;
    }

    public function getServiceCode(): ?string
    {
        return $this->service_code;
    }

    public function setServiceCode(?string $service_code): self
    {
        $this->service_code = $service_code;

        return $this;
    }

    public function getServiceNom(): ?string
    {
        return $this->service_nom;
    }

    public function setServiceNom(?string $service_nom): self
    {
        $this->service_nom = $service_nom;

        return $this;
    }

    public function getServiceGestionEGMT(): ?bool
    {
        return $this->service_gestion_EGMT;
    }

    public function setServiceGestionEGMT(bool $service_gestion_EGMT): self
    {
        $this->service_gestion_EGMT = $service_gestion_EGMT;

        return $this;
    }

    public function getServiceServiceActif(): ?bool
    {
        return $this->service_service_actif;
    }

    public function setServiceServiceActif(bool $service_service_actif): self
    {
        $this->service_service_actif = $service_service_actif;

        return $this;
    }

    public function getEmetteurEdi(): ?bool
    {
        return $this->emetteur_edi;
    }

    public function setEmetteurEdi(bool $emetteur_edi): self
    {
        $this->emetteur_edi = $emetteur_edi;

        return $this;
    }

    public function getRecepteurEdi(): ?bool
    {
        return $this->recepteur_edi;
    }

    public function setRecepteurEdi(bool $recepteur_edi): self
    {
        $this->recepteur_edi = $recepteur_edi;

        return $this;
    }

    public function getGestionStatutMiseEnPaiement(): ?bool
    {
        return $this->gestion_statut_mise_en_paiement;
    }

    public function setGestionStatutMiseEnPaiement(bool $gestion_statut_mise_en_paiement): self
    {
        $this->gestion_statut_mise_en_paiement = $gestion_statut_mise_en_paiement;

        return $this;
    }

    public function getGestionEngagement(): ?bool
    {
        return $this->gestion_engagement;
    }

    public function setGestionEngagement(bool $gestion_engagement): self
    {
        $this->gestion_engagement = $gestion_engagement;

        return $this;
    }

    public function getGestionService(): ?bool
    {
        return $this->gestion_service;
    }

    public function setGestionService(bool $gestion_service): self
    {
        $this->gestion_service = $gestion_service;

        return $this;
    }

    public function getGestionServiceEngagement(): ?bool
    {
        return $this->gestion_service_engagement;
    }

    public function setGestionServiceEngagement(bool $gestion_service_engagement): self
    {
        $this->gestion_service_engagement = $gestion_service_engagement;

        return $this;
    }

    public function getEstMOA(): ?bool
    {
        return $this->est_MOA;
    }

    public function setEstMOA(bool $est_MOA): self
    {
        $this->est_MOA = $est_MOA;

        return $this;
    }

    public function getEstMOAUniquement(): ?bool
    {
        return $this->est_MOA_uniquement;
    }

    public function setEstMOAUniquement(bool $est_MOA_uniquement): self
    {
        $this->est_MOA_uniquement = $est_MOA_uniquement;

        return $this;
    }

    public function getStructureActive(): ?bool
    {
        return $this->structure_active;
    }

    public function setStructureActive(bool $structure_active): self
    {
        $this->structure_active = $structure_active;

        return $this;
    }



   
}
