<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="wannexe", indexes={@ORM\Index(name="wan_numsite", columns={"wan_numsite"})})
 * @ORM\Entity(repositoryClass="App\Repository\WannexeRepository")
 */
class Wannexe
{

    /**
     * @var int
     *
     * @ORM\Column(name="wan_declar", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Doctrine\ORM\Id\IdentityGenerator")
     */
    private $wan_declar;

    /**
     * @ORM\Column(name="wan_numsite", type="integer", nullable=false)
     */
    private $wan_numsite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_raisoc1", type="string", length=55, nullable=true)
     */
    private $wan_raisoc1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_raisoc2", type="string", length=50, nullable=true)
     */
    private $wan_raisoc2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_raisoc3", type="string", length=50, nullable=true)
     */
    private $wan_raisoc3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_numvoie", type="string", length=6, nullable=true)
     */
    private $wan_numvoie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_adresse1", type="string", length=50, nullable=true)
     */
    private $wan_adresse1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_adresse2", type="string", length=50, nullable=true)
     */
    private $wan_adresse2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_adresse3", type="string", length=50, nullable=true)
     */
    private $wan_adresse3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_copos", type="string", length=16, nullable=true)
     */
    private $wan_copos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_ville", type="string", length=42, nullable=true)
     */
    private $wan_ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_tel", type="string", length=26, nullable=true)
     */
    private $wan_tel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_fax", type="string", length=26, nullable=true)
     */
    private $wan_fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wan_website", type="string", length=255, nullable=true)
     */
    private $wan_website;

    /**
     * @var string
     *
     * @ORM\Column(name="wan_nom", type="string", length=50, nullable=false)
     */
    private $wan_nom;

    /**
     * @var string
     *
     * @ORM\Column(name="wan_tit", type="string", length=35, nullable=false)
     */
    private $wan_tit;

    /**
     * @var string
     *
     * @ORM\Column(name="wan_fct", type="string", length=50, nullable=false)
     */
    private $wan_fct;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche01", type="integer", nullable=true)
     */
    private $wan_tranche01;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche02", type="integer", nullable=true)
     */
    private $wan_tranche02;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche03", type="integer", nullable=true)
     */
    private $wan_tranche03;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche04", type="integer", nullable=true)
     */
    private $wan_tranche04;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche05", type="integer", nullable=true)
     */
    private $wan_tranche05;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche06", type="integer", nullable=true)
     */
    private $wan_tranche06;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche07", type="integer", nullable=true)
     */
    private $wan_tranche07;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wan_tranche08", type="integer", nullable=true)
     */
    private $wan_tranche08;
    


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWanDeclar(): ?int
    {
        return $this->wan_declar;
    }

    public function setWanDeclar(int $wan_declar): self
    {
        $this->wan_declar = $wan_declar;

        return $this;
    }

    public function getWanNumsite(): ?int
    {
        return $this->wan_numsite;
    }

    public function setWanNumsite(int $wan_numsite): self
    {
        $this->wan_numsite = $wan_numsite;

        return $this;
    }

    public function getWanRaisoc1(): ?string
    {
        return $this->wan_raisoc1;
    }

    public function setWanRaisoc1(?string $wan_raisoc1): self
    {
        $this->wan_raisoc1 = $wan_raisoc1;

        return $this;
    }

    public function getWanRaisoc2(): ?string
    {
        return $this->wan_raisoc2;
    }

    public function setWanRaisoc2(?string $wan_raisoc2): self
    {
        $this->wan_raisoc2 = $wan_raisoc2;

        return $this;
    }

    public function getWanRaisoc3(): ?string
    {
        return $this->wan_raisoc3;
    }

    public function setWanRaisoc3(?string $wan_raisoc3): self
    {
        $this->wan_raisoc3 = $wan_raisoc3;

        return $this;
    }

    public function getWanNumvoie(): ?string
    {
        return $this->wan_numvoie;
    }

    public function setWanNumvoie(?string $wan_numvoie): self
    {
        $this->wan_numvoie = $wan_numvoie;

        return $this;
    }

    public function getWanAdresse1(): ?string
    {
        return $this->wan_adresse1;
    }

    public function setWanAdresse1(?string $wan_adresse1): self
    {
        $this->wan_adresse1 = $wan_adresse1;

        return $this;
    }

    public function getWanAdresse2(): ?string
    {
        return $this->wan_adresse2;
    }

    public function setWanAdresse2(?string $wan_adresse2): self
    {
        $this->wan_adresse2 = $wan_adresse2;

        return $this;
    }

    public function getWanAdresse3(): ?string
    {
        return $this->wan_adresse3;
    }

    public function setWanAdresse3(?string $wan_adresse3): self
    {
        $this->wan_adresse3 = $wan_adresse3;

        return $this;
    }

    public function getWanCopos(): ?string
    {
        return $this->wan_copos;
    }

    public function setWanCopos(?string $wan_copos): self
    {
        $this->wan_copos = $wan_copos;

        return $this;
    }

    public function getWanVille(): ?string
    {
        return $this->wan_ville;
    }

    public function setWanVille(?string $wan_ville): self
    {
        $this->wan_ville = $wan_ville;

        return $this;
    }

    public function getWanTel(): ?string
    {
        return $this->wan_tel;
    }

    public function setWanTel(?string $wan_tel): self
    {
        $this->wan_tel = $wan_tel;

        return $this;
    }

    public function getWanFax(): ?string
    {
        return $this->wan_fax;
    }

    public function setWanFax(?string $wan_fax): self
    {
        $this->wan_fax = $wan_fax;

        return $this;
    }

    public function getWanWebsite(): ?string
    {
        return $this->wan_website;
    }

    public function setWanWebsite(?string $wan_website): self
    {
        $this->wan_website = $wan_website;

        return $this;
    }

    public function getWanNom(): ?string
    {
        return $this->wan_nom;
    }

    public function setWanNom(?string $wan_nom): self
    {
        $this->wan_nom = $wan_nom;

        return $this;
    }

    public function getWanTit(): ?string
    {
        return $this->wan_tit;
    }

    public function setWanTit(?string $wan_tit): self
    {
        $this->wan_tit = $wan_tit;

        return $this;
    }

    public function getWanFct(): ?string
    {
        return $this->wan_fct;
    }

    public function setWanFct(?string $wan_fct): self
    {
        $this->wan_fct = $wan_fct;

        return $this;
    }

    public function getWanTranche01(): ?int
    {
        return $this->wan_tranche01;
    }

    public function setWanTranche01(?int $wan_tranche01): self
    {
        $this->wan_tranche01 = $wan_tranche01;

        return $this;
    }

    public function getWanTranche02(): ?int
    {
        return $this->wan_tranche02;
    }

    public function setWanTranche02(?int $wan_tranche02): self
    {
        $this->wan_tranche02 = $wan_tranche02;

        return $this;
    }

    public function getWanTranche03(): ?int
    {
        return $this->wan_tranche03;
    }

    public function setWanTranche03(?int $wan_tranche03): self
    {
        $this->wan_tranche03 = $wan_tranche03;

        return $this;
    }

    public function getWanTranche04(): ?int
    {
        return $this->wan_tranche04;
    }

    public function setWanTranche04(?int $wan_tranche04): self
    {
        $this->wan_tranche04 = $wan_tranche04;

        return $this;
    }

    public function getWanTranche05(): ?int
    {
        return $this->wan_tranche05;
    }

    public function setWanTranche05(?int $wan_tranche05): self
    {
        $this->wan_tranche05 = $wan_tranche05;

        return $this;
    }

    public function getWanTranche06(): ?int
    {
        return $this->wan_tranche06;
    }

    public function setWanTranche06(?int $wan_tranche06): self
    {
        $this->wan_tranche06 = $wan_tranche06;

        return $this;
    }
    
    
    public function getWanTranche07(): ?int
    {
        return $this->wan_tranche07;
    }
    
    public function setWanTranche07(?int $wan_tranche07): self
    {
        $this->wan_tranche07 = $wan_tranche07;
    
        return $this;
    }
    
    
    public function getWanTranche08(): ?int
    {
        return $this->wan_tranche08;
    }
    
    public function setWanTranche08(?int $wan_tranche08): self
    {
        $this->wan_tranche08 = $wan_tranche08;
    
        return $this;
    }
}
