<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WaboPublication
 *
 * @ORM\Table(name="wabo_publication", indexes={@ORM\Index(name="wabop_wabo_id", columns={"wabop_wabo_id"})})
 * @ORM\Entity
 */
class WaboPublication
{
    /**
     * @var int
     *
     * @ORM\Column(name="wabop_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wabopId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wabop_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wabopStamp = 'CURRENT_TIMESTAMP';
    
    /**
     * @var string
     *
     * @ORM\Column(name="wabop_titre", type="string", length=255, nullable=false)
     */
    private $wabopTitre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wabop_auteur", type="string", length=255, nullable=true)
     */
    private $wabopAuteur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wabop_editeur", type="string", length=255, nullable=true)
     */
    private $wabopEditeur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wabop_type", type="string", length=255, nullable=true)
     */
    private $wabopType;
    

    /**
     * @var int|null
     *
     * @ORM\Column(name="wabop_nombre", type="integer", nullable=true)
     */
    private $wabopNombre;

    /**
     * @var \Wabo
     *
     * @ORM\ManyToOne(targetEntity="Wabo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wabop_wabo_id", referencedColumnName="wabo_id")
     * })
     */
    private $wabopWabo;

    public function getWabopId(): ?int
    {
        return $this->wabopId;
    }
    
    public function getWabopStamp(): ?\DateTimeInterface
    {
        return $this->wabopStamp;
    }
    
    public function setWabopStamp(\DateTimeInterface $wabopStamp): self
    {
        $this->wabopStamp = $wabopStamp;
    
        return $this;
    }
    
    public function getWabopTitre(): ?string
    {
        return $this->wabopTitre;
    }

    public function setWabopTitre(string $wabopTitre): self
    {
        $this->wabopTitre = $wabopTitre;

        return $this;
    }

    public function getWabopAuteur(): ?string
    {
        return $this->wabopAuteur;
    }

    public function setWabopAuteur(?string $wabopAuteur): self
    {
        $this->wabopAuteur = $wabopAuteur;

        return $this;
    }

    public function getWabopEditeur(): ?string
    {
        return $this->wabopEditeur;
    }

    public function setWabopEditeur(?string $wabopEditeur): self
    {
        $this->wabopEditeur = $wabopEditeur;

        return $this;
    }

    public function getWabopType(): ?string
    {
        return $this->wabopType;
    }

    public function setWabopType(?string $wabopType): self
    {
        $this->wabopType = $wabopType;

        return $this;
    }

    public function getWabopWabo(): ?Wabo
    {
        return $this->wabopWabo;
    }

    public function setWabopWabo(?Wabo $wabopWabo): self
    {
        $this->wabopWabo = $wabopWabo;

        return $this;
    }
    public function getWabopNombre(): ?int
    {
        return $this->wabopNombre;
    }
    
    public function setWabopNombre(?int $wabopNombre): self
    {
        $this->wabopNombre = $wabopNombre;
    
        return $this;
    }

}
