<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200222171243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wdeclar DROP FOREIGN KEY FK_DCA7A4F5F5B61815');
        $this->addSql('DROP INDEX IDX_DCA7A4F5F5B61815 ON wdeclar');
        $this->addSql('ALTER TABLE wdeclar CHANGE wde_typcont wde_typcont VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wdeclar CHANGE wde_typcont wde_typcont VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE wdeclar ADD CONSTRAINT FK_DCA7A4F5F5B61815 FOREIGN KEY (wde_typcont) REFERENCES wtype (label) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_DCA7A4F5F5B61815 ON wdeclar (wde_typcont)');
    }
}
