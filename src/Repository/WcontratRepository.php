<?php

namespace App\Repository;

use App\Entity\Wcontrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wcontrat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wcontrat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wcontrat[]    findAll()
 * @method Wcontrat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WcontratRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wcontrat::class);
    }

}
