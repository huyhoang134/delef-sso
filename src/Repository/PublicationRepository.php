<?php

namespace App\Repository;

use App\Entity\Publication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
/**
 * @method Chorus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chorus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chorus[]    findAll()
 * @method Chorus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PublicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Publication::class);
    }

    
    public function findPublicationBykeyword($value)
    {
       /* $query=$this->createQueryBuilder('p')
        ->andWhere('p.publicationTitre like :val')
        ->setParameter('val', $value.'%')
        ->orderBy('p.publicationTitre', 'ASC')*/
        /*->setMaxResults(100)*/
        /*->groupBy('p.publicationTitre')
        ->getQuery();
        $res=$query->getResult();*/
        
        $em= $this->createQueryBuilder('p')->getQuery()->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('value', 'value');
        $sql="
            SELECT publication_titre COLLATE utf8_general_ci as value from  publication where publication_titre like '".$value."%'

            order by value asc
            ";
        $query = $em->createNativeQuery($sql, $rsm);

        $results = $query->getResult();
        return $results;
    }

    
    public function findPublicationFusionIproBykeyword($value)
    {
      
        
        $em= $this->createQueryBuilder('p')->getQuery()->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('value', 'value');
        $sql="
            SELECT publication_titre COLLATE utf8_general_ci as value from  publication where publication_titre like '".$value."%'
            union 
            SELECT titre as value FROM `repertoire_ipro`  where titre like '".$value."%' 
            
            order by value asc
            ";
        $query = $em->createNativeQuery($sql, $rsm);

        $results = $query->getResult();
        return $results;
    }
    
    public function findPublicationCopieNumWebBykeyword($value)
    {
    
    
        $em= $this->createQueryBuilder('p')->getQuery()->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('value', 'value');
        $sql="
            SELECT publications COLLATE utf8_general_ci as value from  repertoire_copies_num_web_reseaux where publications like '".$value."%'
            order by value asc
            ";
        $query = $em->createNativeQuery($sql, $rsm);
    
        $results = $query->getResult();
        return $results;
    }
    
    public function findPublicationCopieNumCibleBykeyword($value)
    {
    
    
        $em= $this->createQueryBuilder('p')->getQuery()->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('value', 'value');
        $sql="
            SELECT publications COLLATE utf8_general_ci as value from  repertoire_copies_num_ext_ciblees where publications like '".$value."%'

            order by value asc
            ";
        $query = $em->createNativeQuery($sql, $rsm);
    
        $results = $query->getResult();
        return $results;
    }
    
}
