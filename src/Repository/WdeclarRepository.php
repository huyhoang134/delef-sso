<?php

namespace App\Repository;

use App\Entity\Wdeclar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wdeclar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wdeclar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wdeclar[]    findAll()
 * @method Wdeclar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WdeclarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wdeclar::class);
    }

    // /**
    //  * @return Wdeclar[] Returns an array of Wdeclar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wdeclar
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
