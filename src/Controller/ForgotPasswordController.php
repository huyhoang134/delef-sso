<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ForgotPasswordController extends AbstractController
{

    public function __construct(\Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        $this->mailer = $mailer;
        $this->params = $params;
    }

    /**
     * @Route("/codes-acces-oublies", name="forgot_password")
     */
    public function index(Request $request)
    {
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');
        
        /** @var FormInterface $form */
        $form = $this->createFormBuilder([])
            ->add('type_etablissement', ChoiceType::class, [
            'choices' => [
                'Vous êtes :' => '',
                'Entreprise ou administration' => '6',
                'Établissement d’enseignement du 1er degré' => '1',
                'Établissement d’enseignement secondaire' => '2',
                'Établissement d’enseignement supérieur' => '3',
                'Organisme de formation' => '4',
                'Copies-service / entreprise de reprographie' => '5',
                
            ],
            'label' => 'TYPE D\'ÉTABLISSEMENT / ORGANISATION*',
            'required' => true
        ]
        )
            ->add('nom_etablissement', TextType::class, [
            'label' => 'NOM ÉTABLISSEMENT / ORGANISATION*',
            'required' => true
        ])
            ->add('adresse_1', TextType::class, [
            'label' => 'ADRESSE','required' => false
        ])
            ->add('adresse_2', TextType::class, [
            'label' => ' ','required' => false
        ])
            ->add('adresse_3', TextType::class, [
            'label' => ' ', 'required' => false
        ])
            ->add('code_postal', TextType::class, [
            'label' => 'CODE POSTAL*',
            'required' => true
        ])
            ->add('ville', TextType::class, [
            'label' => 'VILLE*',
            'required' => true
        ])
            ->add('email', EmailType::class, [
            'label' => 'EMAIL*',
            'required' => true
        ])
            ->add('submit', SubmitType::class, [
            'label' => 'Valider'
        ])
            ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            $mail_content = $this->renderView('emails/forgot_password.html.twig', [
                'data' => $data
            ]);
            switch ($data["type_etablissement"]) {
                case 1:
                    $recipients = "enseignement-primaire@cfcopies.com";
                    break;
                case 2:
                    $recipients = "enseignement-secondaire@cfcopies.com";
                    break;
                case 3:
                    $recipients = "enseignement-superieur@cfcopies.com";
                    break;
                case 4:
                    $recipients = "formation@cfcopies.com";
                    break;
                case 5:
                    $recipients = "dea@cfcopies.com";
                    break;
                case 6:
                    $recipients = "n.louadjed@cfcopies.com";
                    break;
            }
            
            //$recipients = 'tbourdin@partitech.com';
            $message = (new \Swift_Message('Extranet CFC - Demande de mots de passe'))
                ->setFrom($from_email, $from_name)
                ->setTo($recipients)
                ->setBody($mail_content, 'text/html');
            
            $this->mailer->send($message);
            return $this->redirectToRoute('forgot_password_success');
        }
        
        return $this->render('forgot_password/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/codes-acces-oublies-confirmation", name="forgot_password_success")
     */
    public function success(Request $request)
    {
        return $this->render('forgot_password/success.html.twig', [
            
        ]);
    }
}
