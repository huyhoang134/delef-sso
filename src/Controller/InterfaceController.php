<?php

namespace App\Controller;

use App\Services\CustomerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;


use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;
use App\Entity\Wcontact;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class InterfaceController extends AbstractController
{
    /**
     * @var Request|null
     */
    private $request;
    /**
     * @var CustomerService
     */
    private $customerService;
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        RequestStack $requestStack,
        CustomerService $customerService,
        ParameterBagInterface $params,
        UrlGeneratorInterface $urlGenerator,
        AuthenticationManagerInterface $authManager,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage
    ) {
        $this->request         = $requestStack->getCurrentRequest();
        $this->customerService = $customerService;
        $this->params          = $params;
        $this->urlGenerator    = $urlGenerator;
        $this->authManager     = $authManager;
        $this->tokenStorage    = $tokenStorage;
        
   
    }

    /**
     * @Route("/interface-ouverte", methods={"GET","POST"}, name="interface_open")
     */
    public function interfaceOpen(Request $request, UserInterface $wcocon= NULL)
    {
        $wdeclar=$wcocon->getWdeclars();
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());

        $wtype=$contract['wtype'];
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $declaration_conf_chained = $WcoconRepository->getDeclarationConfiguration($wcocon,false,false);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $contratsChained=$WcoconRepository->getListContratsChained($declaration_conf_chained);
        //on a besoin des contrats indépendant en mode restreint login/password
        $contratsChained_restreint=$WcoconRepository->getListContratsChained($declaration_conf);
        $contratsChained["independant"]=$contratsChained_restreint["independant"];
        
        /*fix pour chainer les boutons lorsque les contrats sont de meme type*/
        $fix_type_contrat=[];
        foreach($contratsChained['independant'] as $i=>$c)
        {
            if($i>0){
                $fix_type_contrat[]=$c->getWconfTypcont()->getId();
            }
        
        }
        
        if(count($contratsChained['independant'])-1>count(array_unique($fix_type_contrat)))
        {
            //on est dans un cas ou les contrats sont les meme donc on les chaines.
            foreach($contratsChained['independant'] as $i=>$c)
            {
                if($i==0){
                    $c->chaine_force=1;
                    $contratsChained['chaine'][]=$c;
                }else{
                    $c->chaine_force=1;
                    $contratsChained['chaine'][]=$c;
                    unset($contratsChained['independant'][$i]);
                }
        
            }
        }
   

        $has_status_p=false;
        foreach($declaration_conf as $c){
            if( $c[0]->type_etape=='declaration' &&  in_array($c[0]->wde_etat_declar, ['P','W'])){
                $has_status_p=true;
            }
            if(  in_array($c[0]->wde_etat_declar, ['W'])){
               // return $this->redirectToRoute('interface_bdc_success');
            }
        }
        
        //si on a qu'un seul contrat chainé, mettons le en contrat indépendant.
        if(count($contratsChained['chaine'])==1){
            
            $contratsChained['independant'][]=$contratsChained['chaine'][0];
            unset($contratsChained['chaine'][0]);
        }

        if($has_status_p){
            return $this->redirectToRoute('interface_bdc');
        }


        if(!empty($contratsChained)){
            return $this->render('interface/interface-open-multi.html.twig', [
                'wdeclar' => $wdeclar[0],
                'wtype'   => $wtype,
                'contrats'=> $contrats,
                'contrats_chained'=> $contratsChained,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
            ]);
        }else{
            return $this->render('interface/interface-open.html.twig', [
                'wdeclar' => $wdeclar[0],
                'wtype'   => $wtype,
                'contrats'=> $contrats,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
            ]);
        }

    }
    
    
    /**
     * @Route("/interface/bon-de-commande", methods={"GET","POST"}, name="interface_bdc")
     */
    public function Bdc(Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/
        

        
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $declaration_conf_chained = $WcoconRepository->getDeclarationConfiguration($wcocon,false,false);
        
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $contratsChained=$WcoconRepository->getListContratsChained($declaration_conf_chained);
        
       
        
        //on a besoin des contrats indépendant en mode restreint login/password
        $contratsChained_restreint=$WcoconRepository->getListContratsChained($declaration_conf);
        $contratsChained["independant"]=$contratsChained_restreint["independant"];
        

        
        
        
        
        $has_status_p=false;
        $BDC_list=[];
        $BDC_panorama_info=[];
        foreach($declaration_conf as $c){
            if(in_array($c[0]->wde_etat_declar, ['P', 'W'/**/])){
                if(!$has_status_p){
                    $has_status_p=$c[0];
                }

                foreach($c as $item){
                    if(in_array($item->type_object,['chorus_pro_extended','chorus_pro', 'panorama_press_numerique', 'bon_commande'])){
                        $wde_order_number=$WcoconRepository->getValue("wde_order_number", $c[0]->wde_dossier, $c[0]->wde_declar);
                       
                        if(in_array($item->type_object,['chorus_pro_extended','chorus_pro', 'bon_commande'])){
                            $BDC_list[]=[
                                'type_object'=>'chorus_pro',
                                'wde_annee'=>$c[0]->wde_annee,
                                'label_annee'=>$c[0]->label_annee,
                                'wde_dossier'=>$c[0]->wde_dossier,
                                'wde_declar'=>$c[0]->wde_declar,
                                'wde_contrat'=>$c[0]->wde_contrat,
                                'wde_etat_declar'=>$c[0]->wde_etat_declar,
                                'label_interface'=>$c[0]->label_interface,
                                'wde_order_number'=>$wde_order_number
                            ];
                            
                        }
                        
                        if(in_array($item->type_object,['panorama_press_numerique']))
                        {
                            $wde_order_number=$WcoconRepository->getValue("panorama_press", $c[0]->wde_dossier, $c[0]->wde_declar);
                            $panorama_bdc=[];
                            foreach($wde_order_number['wpanoramapress_liste'] as $i=>$ppl){
                                $panorama_bdc[]=[
                                    'wppl_order_number'=>$ppl['wppl_order_number'],
                                    'wppl_id'=>$ppl['wppl_id'],
                                    'wppl_label'=>$ppl['wppl_label'],
                                    'wppl_index'=>$i+1,
                                ];
                                $BDC_panorama_info[$ppl['wppl_id']]=$ppl['wppl_label'];
                            }
                            $BDC_list[]=[
                                'type_object'=>'panorama_press',
                                'wde_annee'=>$c[0]->wde_annee,
                                'label_annee'=>$c[0]->label_annee,
                                'wde_dossier'=>$c[0]->wde_dossier,
                                'wde_declar'=>$c[0]->wde_declar,
                                'wde_contrat'=>$c[0]->wde_contrat,
                                'wde_etat_declar'=>$c[0]->wde_etat_declar,
                                'label_interface'=>$c[0]->label_interface,
                                'wde_order_number'=>$panorama_bdc
                            ];
                            
                        }
                    }
                    
                
                }
                
            }
        }
       
        
        if(empty($has_status_p)){
            return $this->redirectToRoute('interface_open');
        }
      

        
        $wdeclar_id=$has_status_p->wde_declar;
        $has_status_p_wdeclar_object=$WcoconRepository->getDeclarById($wdeclar_id);
        $em=$this->getDoctrine()->getManager();
        
        $submit_data = $request->request->all();
        
        if(!empty($submit_data)){
            if(!empty($submit_data['panorama'])){
                $have_empty_bdc=false;
                $have_some_bdc=false;
                foreach($submit_data['panorama'] as $wdeclar=>$panoramas){
                    
                    foreach($panoramas as $wppl=>$wppl_bdc){
                        if(empty(trim($wppl_bdc)))
                        {
                            $have_empty_bdc=true;
                        }else{
                            $have_some_bdc=true;
                            $WcoconRepository->update_BDC_press_liste($wppl, $wppl_bdc,  $wcocon->getWcoDossier(), $wdeclar);
                        }
                        
                    }
                }
                
                $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
               
                if(empty($have_empty_bdc)){
                    $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                    $type_contrat=$wdeclar_object->getWdeTypcont();
                    $mail_content = $this->renderView(
                        'emails/notification_bdc.html.twig', [
                            'wdeclar' => $wdeclar_object,
                            'type_contrat'=>$type_contrat,
                            'panoramas'=>$panoramas,
                            'BDC_panorama_info'=>$BDC_panorama_info,
                        ]
                        );
                    $from_email = $params->get('delef_from_email');
                    $from_name = $params->get('delef_from_name');
                    $wtype = $this->getDoctrine()->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$type_contrat]);
                    $contact_email=$wtype->getContactEmail();
                    //$contact_email="tbourdin@partitech.com";
                    $message = (new \Swift_Message('DELEF réception de bon de commande'))
                    ->setFrom($from_email, $from_name)
                    ->setTo($contact_email);
                    $message->setBody($mail_content, 'text/html');
                    $ret=$mailer->send($message);
                    
                }

                
                if(empty($have_empty_bdc)){
                    
                    $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                    $type_contrat=$wdeclar_object->getWdeTypcont();
                    $wdeclar_object->setWdeEtatDeclar('W');
                    $wdeclar_object->setWdeDateValidation(new \DateTime());
                    $em->persist($wdeclar_object);
                    $em->flush();
 
                }

            }
            
            if(!empty($submit_data['bdc'])){
                foreach($submit_data['bdc'] as $wdeclar=>$bdc){
                    if(!empty(trim($bdc))){
                        $WcoconRepository->update_BDC_declar($bdc,  $wcocon->getWcoDossier(), $wdeclar);
                        
                        
                        $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                        $type_contrat=$wdeclar_object->getWdeTypcont();
                        $mail_content = $this->renderView(
                            'emails/notification_bdc.html.twig', [
                                'wdeclar' => $wdeclar_object,
                                'type_contrat'=>$type_contrat,
                                'bdc'=>$bdc,
                            ]
                            );
                        $from_email = $params->get('delef_from_email');
                        $from_name = $params->get('delef_from_name');
                        $wtype = $this->getDoctrine()->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$type_contrat]);
                        $contact_email=$wtype->getContactEmail();
                        //$contact_email="tbourdin@partitech.com";
                        $message = (new \Swift_Message('DELEF réception de bon de commande'))
                        ->setFrom($from_email, $from_name)
                        ->setTo($contact_email)
                        ->setBody($mail_content, 'text/html');
                        $ret=$mailer->send($message);
                        
                        $wdeclar_object=$WcoconRepository->getDeclarById($wdeclar);
                        $type_contrat=$wdeclar_object->getWdeTypcont();
                        $wdeclar_object->setWdeEtatDeclar('W');
                        $wdeclar_object->setWdeDateValidation(new \DateTime());
                        $em->persist($wdeclar_object);
                        $em->flush();
                    }
                    
                    
                    
                    
                    
                }
            }
            //return $this->redirectToRoute('interface_bdc_success');
            return $this->redirectToRoute('interface_bdc');
        }
        

        
        return $this->render('interface/invoice/invoice_validate.html.twig', [
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            
            'contrats'=> $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            'status_p_label' => $has_status_p->label_interface,
            'status_p_contrat' => $has_status_p->wde_contrat,
            'contrats_chained'=> $contratsChained,
            'BDC_list'=>$BDC_list
            
        ]);
        
    }
    
    /**
     * @Route("/interface/bon-de-commande-confirme", methods={"GET","POST"}, name="interface_bdc_success")
     */
    public function BdcSuccess(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }
    */
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        
        return $this->render('interface/invoice/invoice_validate_success.html.twig', [
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            'contrats'=> $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
        ]);
    }
    
    
    /**
     * @Route("/interface-ferme", methods={"GET","POST"}, name="interface_closed")
     */
    public function InterfaceClosed(Request $request, UserInterface $wcocon = NULL)
    {
        /*$cookieLogoutChecked = $this->request->get('cookieLogoutChecked', null);
        $cookieChecked       = $this->request->get('cookieChecked', null);

        if (!$cookieChecked) {
            //check cookie
            $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->params->get('webapp_key'));
            $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->params->get('webapp_secret'));
            $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('interface_open',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_cache_saved_check') . '?' . $postAppKeyParam . $postAppSecretParam . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        if ($cookieLogoutChecked) {
            return $this->redirectToRoute('security_logout');
        }*/

        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];

        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        

        return $this->render('interface/interface-closed.html.twig', [
            'wdeclar'  => $wdeclar,
            'wtype'    => $wtype,
            'contrats' => $contrats,
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
            'primary_declaration'=>$primary_declaration,
        ]);
    }
    


    /**
     * @Route("/interface/contact", methods={"GET","POST"}, name="interface_contact")
     */
    public function InterfaceContact(Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer)
    {
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $contract=$WcoconRepository->getContract($wcocon->getUsername());
        $wtype=$contract['wtype'];
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        $contrats=$WcoconRepository->getListContrats($declaration_conf);

        $submit_data = $request->request->all();

        $f = $this->createFormBuilder([]);
        
        $f->add('civilite', ChoiceType::class, [
            'choices' => [
                'Madame' => 'Madame',
                'Monsieur' => 'Monsieur',
            ],
            'label' => 'Civilité',
            'required' => true,
            'expanded' => true
        ]
            )
            ->add('nom', TextType::class, [
                'label' => 'Nom',
                'required' => true
            ])
            ->add('fonction', TextType::class, [
                'label' => 'Fonction'
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true
            ])
            
            ->add('etablissement', TextType::class, [
                'label' => 'Etablissement',
                'required' => true,
                'data' => !empty($submit_data['form'])?$submit_data['form']['etablissement']:$wcocon->getWcoRaisoc1().' '.$wcocon->getWcoRaisoc2()
            ])
            
            ->add('code_postal', IntegerType::class, [
                'label' => 'CODE POSTAL',
                'required' => true,
                'attr' => ['maxlength' => '5','min' => '1','maxlength' => '99999'],
                'data' => !empty($submit_data['form'])?$submit_data['form']['code_postal']:$wcocon->getWcoCopos()
            ])
            ->add('ville', TextType::class, [
                'label' => 'VILLE',
                'required' => true,
                'data' => !empty($submit_data['form'])?$submit_data['form']['ville']:$wcocon->getWcoVille()
            ])

            ->add('sujet', TextType::class, [
                'label' => 'Sujet',
                'required' => true
            ])
            
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'required' => true,
                'attr' =>["rows"=>"6", "cols"=>"60"]
            ])
            
            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
            
            $form = $f->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submit_data = $request->request->all();
            
            $from_email = $params->get('delef_from_email');
            $from_name = $params->get('delef_from_name');
            
            $data = $form->getData();
            
            $mail_content = $this->renderView('emails/contact.html.twig', [
                'data' => $data
            ]);
            
            $recipients = $wtype->getContactEmail();
            //$recipients = 'tbourdin@partitech.com';
            $message = (new \Swift_Message('Extranet CFC - Contact - '.$data['sujet']))
            ->setFrom($from_email, $from_name)
            ->setTo($recipients)
            ->setBody($mail_content, 'text/html');
            
            $mailer->send($message);
            return $this->render('interface/interface-contact-success.html.twig', [
                'primary_declaration'=>$primary_declaration,
                'wdeclar' => $wdeclar,
                'wtype'   => $wtype,
                'contrats'  => $contrats,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
            ]);

        }
        
        
        
        return $this->render('interface/interface-contact.html.twig', [
            'primary_declaration'=>$primary_declaration,
            'wdeclar' => $wdeclar,
            'wtype'   => $wtype,
            'contrats'  => $contrats,
            'form'  => $form->createView(),
            'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
        ]);
    }
       
    private function getHistoryFiles($primary_declaration, $WtypeCont , $kernel){
    
        $name = $projectRoot = $kernel->getProjectDir()."/public/pdf";
        $contrat = $primary_declaration->getWdeContrat();
         
        $status = strtoupper($primary_declaration->getWdeEtatDeclar());
        $annee = $primary_declaration->getWdeAnnee();
        $type = str_replace(' / ', '', $WtypeCont->getLabel());
    
        $arr = $this->list_dir($name, $type, $contrat, $status, $annee);
        krsort($arr);
        $i=0;
        foreach($arr as $year=>$file){
            if($i>=5){
                unset($arr[$year]);
            }
            $i++;
        }
        return $arr;
    }
    
    private function list_dir($name, $type, $contrat, $status, $annee, $arr = array())
    {

        $type = str_replace(' / ', '', $type);
        $type = str_replace('/', '', $type);
        $type = str_replace(' ', '', $type);
        if ($dir = opendir($name)) {
            while ($file = readdir($dir)) {
                if (in_array($file, array(".", "..")))
                    continue;
    
                    if (is_dir($name . '/' . $file)) {
                        $arr = $this->list_dir($name . '/' . $file, $type, $contrat, $status, $annee, $arr);
                    } elseif ($file == $type . '_' . $contrat . '.pdf') {
                        $expl=explode('/', $name);
                        $key = end($expl);
    
                        if ($status == 'V' && $annee == $key)
                            continue;
                            $arr[$key] = $name . '/' . $file;
                    }
            }
            closedir($dir);
        }
        return $arr;
    }
}
